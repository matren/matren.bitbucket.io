(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "+EN/":
/*!*************************!*\
  !*** ./src/styles.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "LboF");
            var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--13-1!../node_modules/postcss-loader/src??embedded!../node_modules/resolve-url-loader??ref--13-3!../node_modules/sass-loader/dist/cjs.js??ref--13-4!./styles.scss */ "/I9Y");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "/I9Y":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--13-1!./node_modules/postcss-loader/src??embedded!./node_modules/resolve-url-loader??ref--13-3!./node_modules/sass-loader/dist/cjs.js??ref--13-4!./src/styles.scss ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "JPst");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "/* You can add global styles to this file, and also import other style files */\nbody {\n  background-color: #202026;\n  color: #aaa;\n  font-family: \"Roboto Mono\";\n  margin: 0;\n}\n*, *:before, *:after {\n  box-sizing: border-box;\n}\n/* breakpoints */\n/* breakpoints */\ninput,\ntextarea,\ninput:-webkit-autofill {\n  font-size: 22px;\n  border: none;\n  padding: 10px 15px;\n  border-radius: 4px;\n  color: #aaa;\n  background-color: #181818;\n  border: 1px solid #181818;\n  font-family: \"Roboto Mono\";\n  -webkit-transition: 0.5s, width 0s, height 0s;\n  transition: 0.5s, width 0s, height 0s;\n  box-shadow: inset 0 4px 4px 0 rgba(0, 0, 0, 0.28), inset 0 6px 2px -4px rgba(0, 0, 0, 0.24), inset 0 2px 10px 0 rgba(0, 0, 0, 0.4);\n}\ninput:focus,\ntextarea:focus,\ninput:-webkit-autofill:focus {\n  outline: none;\n  box-shadow: inset 0 8px 8px 0 rgba(0, 0, 0, 0.28), inset 0 12px 4px -4px rgba(0, 0, 0, 0.24), inset 0 4px 20px 0 rgba(0, 0, 0, 0.4);\n}\ninput:-internal-autofill-selected,\ntextarea:-internal-autofill-selected,\ninput:-webkit-autofill:-internal-autofill-selected {\n  background-color: #181818 !important;\n}\n.btn {\n  background-color: #5f3fe0;\n  border-radius: 4px;\n  border: 2px solid transparent;\n  padding: 6px 12px;\n  margin: 10px;\n  text-align: center;\n  color: #eee;\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  cursor: pointer;\n  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.28), 0 6px 2px -4px rgba(0, 0, 0, 0.28), 0 2px 10px 0 rgba(0, 0, 0, 0.4);\n  transition: 0.5s;\n}\n.btn:hover {\n  background-color: #7e62f0;\n}\n.btn:focus {\n  outline: none;\n  box-shadow: inset 0 4px 4px 0 rgba(0, 0, 0, 0.28), inset 0 6px 2px -4px rgba(0, 0, 0, 0.24), inset 0 2px 10px 0 rgba(0, 0, 0, 0.4);\n}\n.btn.btn-transparent {\n  background-color: transparent;\n  border: 2px solid #5834eb;\n}\n.btn.btn-transparent:hover {\n  background-color: #7e62f0;\n}\n.btn.btn-red {\n  background-color: #b10101;\n}\n.btn.btn-red:hover {\n  background-color: #e40101;\n}", "",{"version":3,"sources":["webpack://src/styles.scss","webpack://src/scss/base.scss","webpack://src/scss/_variables.scss","webpack://src/scss/_input.scss","webpack://src/scss/_mixins.scss","webpack://src/scss/_button.scss"],"names":[],"mappings":"AAAA,8EAAA;ACAA;EACE,yBAAA;EACA,WAAA;EACA,0BAAA;EACA,SAAA;ADEF;ACAA;EACE,sBAAA;ADGF;AEVA,gBAAA;AAAA,gBAAA;ACGA;;;EAGC,eAAA;EACA,YAAA;EACA,kBAAA;EACA,kBAAA;EACA,WAAA;EACA,yBDEU;ECDV,yBAAA;EACA,0BAAA;EACA,6CAAA;EAAA,qCAAA;ECiLA,kIAAA;AJpKD;AGXC;;;EACC,aAAA;ECkLD,mIAAA;AJlKD;AGbC;;;EACC,oCAAA;AHiBF;AKtCA;EACC,yBAAA;EACA,kBAAA;EACA,6BAAA;EACA,iBAAA;EACA,YAAA;EACA,kBAAA;EACC,WAAA;EACD,sCAAA;EACA,eAAA;ED0KA,gHAAA;ECxKA,gBAAA;ALyCD;AKxCC;EACC,yBAAA;AL0CF;AKxCE;EACE,aAAA;ED+KH,kIAAA;AJpID;AKxCC;EACC,6BAAA;EACA,yBAAA;AL0CF;AKzCE;EACC,yBAAA;AL2CH;AKxCE;EACE,yBHbE;AFuDN;AKzCI;EACE,yBAAA;AL2CN","sourcesContent":["/* You can add global styles to this file, and also import other style files */\r\n\r\n@import 'base';\r\n@import 'input';\r\n@import 'button';\r\n\r\n","body {\r\n  background-color: #202026;\r\n  color: #aaa;\r\n  font-family: 'Roboto Mono';\r\n  margin: 0;\r\n}\r\n*, *:before, *:after {\r\n  box-sizing: border-box;\r\n}\r\n","/* breakpoints */\r\n$xs: 0;\r\n$sm: 600px;\r\n$md: 960px;\r\n$lg: 1280px;\r\n$xl: 1920px;\r\n\r\n\r\n\r\n\r\n$c1: #5834eb;\r\n$c3: #00f7ff;\r\n$c2: #4f00c4;\r\n$darkgrey: #181818;\r\n$red: #b10101;\r\n\r\n","@import 'mixins';\r\n@import 'variables';\r\n\r\ninput,\r\ntextarea,\r\ninput:-webkit-autofill {\r\n\tfont-size: 22px;\r\n\tborder: none;\r\n\tpadding: 10px 15px;\r\n\tborder-radius: 4px;\r\n\tcolor: #aaa;\r\n\tbackground-color: $darkgrey;\r\n\tborder: 1px solid $darkgrey;\r\n\tfont-family: 'Roboto Mono';\r\n\ttransition: .5s, width 0s, height 0s;\r\n\t@include boxShadowInset;\r\n\t&:focus {\r\n\t\toutline: none;\r\n\t\t@include boxShadowInsetStrong;\r\n\t}\r\n\t&:-internal-autofill-selected {\r\n\t\tbackground-color: $darkgrey !important;\r\n\t}\r\n}","@import 'variables';\r\n\r\n\r\n@mixin xs {\r\n\t@media (min-width: $xs) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin sm {\r\n\t@media (min-width: $sm) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin md {\r\n\t@media (min-width: $md) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin lg {\r\n\t@media (min-width: $lg) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin xl {\r\n\t@media (min-width: $xl) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin maxxs {\r\n\t@media (max-width: $xs) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin maxsm {\r\n\t@media (max-width: $sm) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin maxmd {\r\n\t@media (max-width: $md) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin maxlg {\r\n\t@media (max-width: $lg) {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin order($val) {\r\n\t-webkit-box-ordinal-group: $val;\r\n\t-moz-box-ordinal-group: $val;\r\n\t-ms-flex-order: $val;\r\n\t-webkit-order: $val;\r\n\torder: $val;\r\n}\r\n@mixin transition($transition...) {\r\n\t-moz-transition: $transition;\r\n\t-o-transition: $transition;\r\n\t-webkit-transition: $transition;\r\n\ttransition: $transition;\r\n}\r\n@mixin transition-property($property...) {\r\n\t-moz-transition-property: $property;\r\n\t-o-transition-property: $property;\r\n\t-webkit-transition-property: $property;\r\n\ttransition-property: $property;\r\n}\r\n@mixin transition-duration($duration...) {\r\n\t-moz-transition-property: $duration;\r\n\t-o-transition-property: $duration;\r\n\t-webkit-transition-property: $duration;\r\n\ttransition-property: $duration;\r\n}\r\n@mixin transition-timing-function($timing...) {\r\n\t-moz-transition-timing-function: $timing;\r\n\t-o-transition-timing-function: $timing;\r\n\t-webkit-transition-timing-function: $timing;\r\n\ttransition-timing-function: $timing;\r\n}\r\n@mixin transition-delay($delay...) {\r\n\t-moz-transition-delay: $delay;\r\n\t-o-transition-delay: $delay;\r\n\t-webkit-transition-delay: $delay;\r\n\ttransition-delay: $delay;\r\n}\r\n@mixin transform($in) {\r\n\ttransform: $in;\r\n\t-webkit-transform: $in;\r\n\t-moz-transform: $in;\r\n\t-o-transform: $in;\r\n\t-ms-transform: $in;\r\n}\r\n@mixin transform-style($type) {\r\n\t-webkit-transform-style: $type;\r\n\t-moz-transform-style: $type;\r\n\t-o-transform-style: $type;\r\n\t-ms-transform-style: $type;\r\n\ttransform-style: $type;\r\n}\r\n@mixin animation($animate...) {\r\n\t$max: length($animate);\r\n\t$animations: '';\r\n\t@for $i from 1 through $max {\r\n\t\t$animations: #{$animations + nth($animate, $i)};\r\n\t\t@if $i < $max {\r\n\t\t\t$animations: #{$animations + \", \"};\r\n\t\t}\r\n\t}\r\n\t-webkit-animation: $animations;\r\n\t-moz-animation: $animations;\r\n\t-o-animation: $animations;\r\n\tanimation: $animations;\r\n}\r\n@mixin animation-duration($time) {\r\n\t-webkit-animation-duration: $time;\r\n\t-moz-animation-duration: $time;\r\n\t-ms-animation-duration: $time;\r\n\t-o-animation-duration: $time;\r\n\tanimation-duration: $time;\r\n}\r\n@mixin keyframes($animationName) {\r\n\t@-webkit-keyframes #{$animationName} {\r\n\t\t@content;\r\n\t}\r\n\t@-moz-keyframes #{$animationName} {\r\n\t\t@content;\r\n\t}\r\n\t@-o-keyframes #{$animationName} {\r\n\t\t@content;\r\n\t}\r\n\t@keyframes #{$animationName} {\r\n\t\t@content;\r\n\t}\r\n}\r\n@mixin noselect() {\r\n\t-webkit-touch-callout: none;\r\n\t/* iOS Safari */\r\n\t-webkit-user-select: none;\r\n\t/* Safari */\r\n\t-khtml-user-select: none;\r\n\t/* Konqueror HTML */\r\n\t-moz-user-select: none;\r\n\t/* Firefox */\r\n\t-ms-user-select: none;\r\n\t/* Internet Explorer/Edge */\r\n\tuser-select: none;\r\n\t/* Non-prefixed version, currently\r\n                                  supported by Chrome and Opera */\r\n}\r\n\r\n\r\n@mixin defBoxShadow() {\r\n\tbox-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);\r\n}\r\n\r\n\r\n@mixin defBoxShadowStrong() {\r\n\tbox-shadow: 0 3px 3px 0 rgba(0, 0, 0, 0.5), 0 4px 2px -3px rgba(0, 0, 0, 0.5), 0 2px 8px 0 rgba(0, 0, 0, 0.4);\r\n}\r\n\r\n@mixin defBoxShadowInset() {\r\n\tbox-shadow: inset 0 2px 2px 0 rgba(0, 0, 0, 0.14), inset 0 3px 1px -2px rgba(0, 0, 0, 0.12), inset 0 1px 5px 0 rgba(0, 0, 0, 0.2);\r\n}\r\n\r\n@mixin defBorder() {\r\n\tborder: 1px solid rgba(0, 0, 0, 0.125);\r\n}\r\n\r\n@mixin defHR() {\r\n\tborder-bottom: 1px solid rgba(0, 0, 0, 0.2);\r\n}\r\n\r\n\r\n@mixin boxShadowLight() {\r\n\tbox-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);\r\n}\r\n\r\n\r\n@mixin boxShadow() {\r\n\tbox-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.28), 0 6px 2px -4px rgba(0, 0, 0, 0.28), 0 2px 10px 0 rgba(0, 0, 0, 0.4);\r\n}\r\n\r\n@mixin boxShadowStrong() {\r\n\tbox-shadow: 0 8px 8px 0 rgba(0, 0, 0, 0.28), 0 12px 4px -8px rgba(0, 0, 0, 0.28), 0 4px 20px 0 rgba(0, 0, 0, 0.4);\r\n}\r\n\r\n@mixin boxShadowInsetLight() {\r\n\tbox-shadow: inset 0 2px 2px 0 rgba(0, 0, 0, 0.14), inset 0 3px 1px -2px rgba(0, 0, 0, 0.12), inset 0 1px 5px 0 rgba(0, 0, 0, 0.2);\r\n}\r\n\r\n@mixin boxShadowInset() {\r\n\tbox-shadow: inset 0 4px 4px 0 rgba(0, 0, 0, 0.28), inset 0 6px 2px -4px rgba(0, 0, 0, 0.24), inset 0 2px 10px 0 rgba(0, 0, 0, 0.4);\r\n}\r\n\r\n@mixin boxShadowInsetStrong() {\r\n\tbox-shadow: inset 0 8px 8px 0 rgba(0, 0, 0, 0.28), inset 0 12px 4px -4px rgba(0, 0, 0, 0.24), inset 0 4px 20px 0 rgba(0, 0, 0, 0.4);\r\n}\r\n\r\n",".btn {\r\n\tbackground-color: desaturate($c1, 10%);\r\n\tborder-radius: 4px;\r\n\tborder: 2px solid transparent;\r\n\tpadding: 6px 12px;\r\n\tmargin: 10px;\r\n\ttext-align: center;\r\n  color: #eee;\r\n\tborder: 1px solid rgba(0, 0, 0, 0.125);\r\n\tcursor: pointer;\r\n\t@include boxShadow;\r\n\ttransition: .5s;\r\n\t&:hover {\r\n\t\tbackground-color: lighten($c1, 10%);\r\n  }\r\n  &:focus {\r\n    outline: none;\r\n    @include boxShadowInset;\r\n  }\r\n\t&.btn-transparent {\r\n\t\tbackground-color: transparent;\r\n\t\tborder: 2px solid $c1;\r\n\t\t&:hover {\r\n\t\t\tbackground-color: lighten($c1, 10%);\r\n\t\t}\r\n  }\r\n  &.btn-red {\r\n    background-color: $red;\r\n    &:hover {\r\n      background-color: lighten($red, 10%);\r\n    }\r\n  }\r\n}"],"sourceRoot":""}]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ 3:
/*!*******************************!*\
  !*** multi ./src/styles.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\typing\src\styles.scss */"+EN/");


/***/ }),

/***/ "JPst":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "LboF":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ })

},[[3,"runtime"]]]);
//# sourceMappingURL=styles-es2015.js.map