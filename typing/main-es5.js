(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "/fqK":
    /*!*****************************************************************************!*\
      !*** ./src/app/modules/notification/notification/notification.component.ts ***!
      \*****************************************************************************/

    /*! exports provided: NotificationComponent */

    /***/
    function fqK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationComponent", function () {
        return NotificationComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function NotificationComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx_r0.message.icon, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        }
      }

      var NotificationComponent = /*#__PURE__*/function () {
        function NotificationComponent() {
          _classCallCheck(this, NotificationComponent);

          this.closeMessage = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        }

        _createClass(NotificationComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onCloseMessage",
          value: function onCloseMessage() {
            this.closeMessage.emit();
          }
        }]);

        return NotificationComponent;
      }();

      NotificationComponent.ɵfac = function NotificationComponent_Factory(t) {
        return new (t || NotificationComponent)();
      };

      NotificationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: NotificationComponent,
        selectors: [["notification"]],
        inputs: {
          message: "message"
        },
        outputs: {
          closeMessage: "closeMessage"
        },
        decls: 6,
        vars: 3,
        consts: [[3, "ngClass"], ["class", "icon", 4, "ngIf"], [1, "message"], [1, "btn-close", 3, "click"], [1, "fa", "fa-times"], [1, "icon"], [3, "src"]],
        template: function NotificationComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NotificationComponent_div_1_Template, 2, 1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NotificationComponent_Template_div_click_4_listener() {
              return ctx.onCloseMessage();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", "notification " + ctx.message.type);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.message.icon);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.message.message, " ");
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"]],
        styles: [".notification[_ngcontent-%COMP%] {\n  position: relative;\n  pointer-events: all;\n  border-radius: 4px;\n  padding: 20px;\n  display: flex;\n  justify-content: flex-start;\n  align-items: center;\n  background-color: #444;\n  position: relative;\n  padding: 10px 12px;\n  width: 100%;\n  min-width: 400px;\n  max-width: 600px;\n  font-size: 22px;\n  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);\n  margin: 10px 0;\n}\n.notification[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%] {\n  z-index: 2;\n  width: 60px;\n}\n.notification[_ngcontent-%COMP%]   .icon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.notification[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%] {\n  padding: 10px;\n  z-index: 1;\n}\n.notification[_ngcontent-%COMP%]   .btn-close[_ngcontent-%COMP%] {\n  background: none;\n  position: absolute;\n  top: 0;\n  right: 0;\n  z-index: 3;\n  font-size: 20px;\n  color: rgba(255, 255, 255, 0.8);\n  height: 40px;\n  width: 40px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background-color: rgba(0, 0, 0, 0.15);\n  cursor: pointer;\n  transition: all 0.5s, border-radius 0.7s ease 0.3s;\n}\n.notification[_ngcontent-%COMP%]   .btn-close[_ngcontent-%COMP%]:hover {\n  border-bottom-left-radius: 4px;\n  background: rgba(255, 255, 255, 0.2);\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9zY3NzL192YXJpYWJsZXMuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9zY3NzL19taXhpbnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBQTtBQUFBLGdCQUFBO0FDR0E7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUN1SUQsK0dBQUE7RURySUMsY0FBQTtBQUFGO0FBQ0U7RUFDRSxVQUFBO0VBQ0EsV0FBQTtBQUNKO0FBQUk7RUFDRSxXQUFBO0FBRU47QUFDRTtFQUNFLGFBQUE7RUFDQSxVQUFBO0FBQ0o7QUFpQkU7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLCtCQUFBO0VBRUEsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHFDQUFBO0VBQ0EsZUFBQTtFQ0hILGtERElHO0FBYko7QUFjSTtFQUNFLDhCQUFBO0VBQ0Esb0NBQUE7RUFDQSxXQUFBO0FBWk4iLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogYnJlYWtwb2ludHMgKi9cclxuJHhzOiAwO1xyXG4kc206IDYwMHB4O1xyXG4kbWQ6IDk2MHB4O1xyXG4kbGc6IDEyODBweDtcclxuJHhsOiAxOTIwcHg7XHJcblxyXG5cclxuXHJcblxyXG4kYzE6ICM1ODM0ZWI7XHJcbiRjMzogIzAwZjdmZjtcclxuJGMyOiAjNGYwMGM0O1xyXG4kZGFya2dyZXk6ICMxODE4MTg7XHJcbiRyZWQ6ICNiMTAxMDE7XHJcblxyXG4iLCJAaW1wb3J0ICd2YXJpYWJsZXMnO1xyXG5AaW1wb3J0ICdtaXhpbnMnO1xyXG5cclxuLm5vdGlmaWNhdGlvbiB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHBvaW50ZXItZXZlbnRzOiBhbGw7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDQ0O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nOiAxMHB4IDEycHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWluLXdpZHRoOiA0MDBweDtcclxuICBtYXgtd2lkdGg6IDYwMHB4O1xyXG4gIGZvbnQtc2l6ZTogMjJweDtcclxuICBAaW5jbHVkZSBkZWZCb3hTaGFkb3c7XHJcbiAgbWFyZ2luOiAxMHB4IDA7XHJcbiAgLmljb24ge1xyXG4gICAgei1pbmRleDogMjtcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5tZXNzYWdlIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gIH1cclxuICAvLyAmLmluZm8ge1xyXG4gIC8vICAgY29sb3I6ICNmZmY7XHJcbiAgLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAkYzE7XHJcbiAgLy8gICBib3JkZXI6IDJweCBzb2xpZCBkYXJrZW4oJGMxLCAxMCUpO1xyXG4gIC8vIH1cclxuICAvLyAmLnN1Y2Nlc3Mge1xyXG4gIC8vICAgY29sb3I6ICNmZmY7XHJcbiAgLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAkYzQ7XHJcbiAgLy8gICBib3JkZXI6IDJweCBzb2xpZCBkYXJrZW4oJGM0LCAxMCUpO1xyXG4gIC8vIH1cclxuICAvLyAmLmVycm9yIHtcclxuICAvLyAgIGNvbG9yOiAjZmZmO1xyXG4gIC8vICAgYmFja2dyb3VuZC1jb2xvcjogJGM1O1xyXG4gIC8vICAgYm9yZGVyOiAycHggc29saWQgZGFya2VuKCRjNSwgMTAlKTtcclxuICAvLyB9XHJcblxyXG4gIC5idG4tY2xvc2Uge1xyXG4gICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgei1pbmRleDogMztcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiByZ2JhKCNmZmYsIC44KTtcclxuICAgIC8vIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCMwMDAsIC4xNSk7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBAaW5jbHVkZSB0cmFuc2l0aW9uKGFsbCAuNXMsIGJvcmRlci1yYWRpdXMgLjdzIGVhc2UgLjNzKTtcclxuICAgICY6aG92ZXIge1xyXG4gICAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA0cHg7XHJcbiAgICAgIGJhY2tncm91bmQ6IHJnYmEoI2ZmZiwgLjIpO1xyXG4gICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuICB9XHJcbn0iLCJAaW1wb3J0ICd2YXJpYWJsZXMnO1xyXG5cclxuXHJcbkBtaXhpbiB4cyB7XHJcblx0QG1lZGlhIChtaW4td2lkdGg6ICR4cykge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBzbSB7XHJcblx0QG1lZGlhIChtaW4td2lkdGg6ICRzbSkge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBtZCB7XHJcblx0QG1lZGlhIChtaW4td2lkdGg6ICRtZCkge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBsZyB7XHJcblx0QG1lZGlhIChtaW4td2lkdGg6ICRsZykge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiB4bCB7XHJcblx0QG1lZGlhIChtaW4td2lkdGg6ICR4bCkge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBtYXh4cyB7XHJcblx0QG1lZGlhIChtYXgtd2lkdGg6ICR4cykge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBtYXhzbSB7XHJcblx0QG1lZGlhIChtYXgtd2lkdGg6ICRzbSkge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBtYXhtZCB7XHJcblx0QG1lZGlhIChtYXgtd2lkdGg6ICRtZCkge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBtYXhsZyB7XHJcblx0QG1lZGlhIChtYXgtd2lkdGg6ICRsZykge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBvcmRlcigkdmFsKSB7XHJcblx0LXdlYmtpdC1ib3gtb3JkaW5hbC1ncm91cDogJHZhbDtcclxuXHQtbW96LWJveC1vcmRpbmFsLWdyb3VwOiAkdmFsO1xyXG5cdC1tcy1mbGV4LW9yZGVyOiAkdmFsO1xyXG5cdC13ZWJraXQtb3JkZXI6ICR2YWw7XHJcblx0b3JkZXI6ICR2YWw7XHJcbn1cclxuQG1peGluIHRyYW5zaXRpb24oJHRyYW5zaXRpb24uLi4pIHtcclxuXHQtbW96LXRyYW5zaXRpb246ICR0cmFuc2l0aW9uO1xyXG5cdC1vLXRyYW5zaXRpb246ICR0cmFuc2l0aW9uO1xyXG5cdC13ZWJraXQtdHJhbnNpdGlvbjogJHRyYW5zaXRpb247XHJcblx0dHJhbnNpdGlvbjogJHRyYW5zaXRpb247XHJcbn1cclxuQG1peGluIHRyYW5zaXRpb24tcHJvcGVydHkoJHByb3BlcnR5Li4uKSB7XHJcblx0LW1vei10cmFuc2l0aW9uLXByb3BlcnR5OiAkcHJvcGVydHk7XHJcblx0LW8tdHJhbnNpdGlvbi1wcm9wZXJ0eTogJHByb3BlcnR5O1xyXG5cdC13ZWJraXQtdHJhbnNpdGlvbi1wcm9wZXJ0eTogJHByb3BlcnR5O1xyXG5cdHRyYW5zaXRpb24tcHJvcGVydHk6ICRwcm9wZXJ0eTtcclxufVxyXG5AbWl4aW4gdHJhbnNpdGlvbi1kdXJhdGlvbigkZHVyYXRpb24uLi4pIHtcclxuXHQtbW96LXRyYW5zaXRpb24tcHJvcGVydHk6ICRkdXJhdGlvbjtcclxuXHQtby10cmFuc2l0aW9uLXByb3BlcnR5OiAkZHVyYXRpb247XHJcblx0LXdlYmtpdC10cmFuc2l0aW9uLXByb3BlcnR5OiAkZHVyYXRpb247XHJcblx0dHJhbnNpdGlvbi1wcm9wZXJ0eTogJGR1cmF0aW9uO1xyXG59XHJcbkBtaXhpbiB0cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbigkdGltaW5nLi4uKSB7XHJcblx0LW1vei10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjogJHRpbWluZztcclxuXHQtby10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjogJHRpbWluZztcclxuXHQtd2Via2l0LXRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOiAkdGltaW5nO1xyXG5cdHRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOiAkdGltaW5nO1xyXG59XHJcbkBtaXhpbiB0cmFuc2l0aW9uLWRlbGF5KCRkZWxheS4uLikge1xyXG5cdC1tb3otdHJhbnNpdGlvbi1kZWxheTogJGRlbGF5O1xyXG5cdC1vLXRyYW5zaXRpb24tZGVsYXk6ICRkZWxheTtcclxuXHQtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6ICRkZWxheTtcclxuXHR0cmFuc2l0aW9uLWRlbGF5OiAkZGVsYXk7XHJcbn1cclxuQG1peGluIHRyYW5zZm9ybSgkaW4pIHtcclxuXHR0cmFuc2Zvcm06ICRpbjtcclxuXHQtd2Via2l0LXRyYW5zZm9ybTogJGluO1xyXG5cdC1tb3otdHJhbnNmb3JtOiAkaW47XHJcblx0LW8tdHJhbnNmb3JtOiAkaW47XHJcblx0LW1zLXRyYW5zZm9ybTogJGluO1xyXG59XHJcbkBtaXhpbiB0cmFuc2Zvcm0tc3R5bGUoJHR5cGUpIHtcclxuXHQtd2Via2l0LXRyYW5zZm9ybS1zdHlsZTogJHR5cGU7XHJcblx0LW1vei10cmFuc2Zvcm0tc3R5bGU6ICR0eXBlO1xyXG5cdC1vLXRyYW5zZm9ybS1zdHlsZTogJHR5cGU7XHJcblx0LW1zLXRyYW5zZm9ybS1zdHlsZTogJHR5cGU7XHJcblx0dHJhbnNmb3JtLXN0eWxlOiAkdHlwZTtcclxufVxyXG5AbWl4aW4gYW5pbWF0aW9uKCRhbmltYXRlLi4uKSB7XHJcblx0JG1heDogbGVuZ3RoKCRhbmltYXRlKTtcclxuXHQkYW5pbWF0aW9uczogJyc7XHJcblx0QGZvciAkaSBmcm9tIDEgdGhyb3VnaCAkbWF4IHtcclxuXHRcdCRhbmltYXRpb25zOiAjeyRhbmltYXRpb25zICsgbnRoKCRhbmltYXRlLCAkaSl9O1xyXG5cdFx0QGlmICRpIDwgJG1heCB7XHJcblx0XHRcdCRhbmltYXRpb25zOiAjeyRhbmltYXRpb25zICsgXCIsIFwifTtcclxuXHRcdH1cclxuXHR9XHJcblx0LXdlYmtpdC1hbmltYXRpb246ICRhbmltYXRpb25zO1xyXG5cdC1tb3otYW5pbWF0aW9uOiAkYW5pbWF0aW9ucztcclxuXHQtby1hbmltYXRpb246ICRhbmltYXRpb25zO1xyXG5cdGFuaW1hdGlvbjogJGFuaW1hdGlvbnM7XHJcbn1cclxuQG1peGluIGFuaW1hdGlvbi1kdXJhdGlvbigkdGltZSkge1xyXG5cdC13ZWJraXQtYW5pbWF0aW9uLWR1cmF0aW9uOiAkdGltZTtcclxuXHQtbW96LWFuaW1hdGlvbi1kdXJhdGlvbjogJHRpbWU7XHJcblx0LW1zLWFuaW1hdGlvbi1kdXJhdGlvbjogJHRpbWU7XHJcblx0LW8tYW5pbWF0aW9uLWR1cmF0aW9uOiAkdGltZTtcclxuXHRhbmltYXRpb24tZHVyYXRpb246ICR0aW1lO1xyXG59XHJcbkBtaXhpbiBrZXlmcmFtZXMoJGFuaW1hdGlvbk5hbWUpIHtcclxuXHRALXdlYmtpdC1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG5cdEAtbW96LWtleWZyYW1lcyAjeyRhbmltYXRpb25OYW1lfSB7XHJcblx0XHRAY29udGVudDtcclxuXHR9XHJcblx0QC1vLWtleWZyYW1lcyAjeyRhbmltYXRpb25OYW1lfSB7XHJcblx0XHRAY29udGVudDtcclxuXHR9XHJcblx0QGtleWZyYW1lcyAjeyRhbmltYXRpb25OYW1lfSB7XHJcblx0XHRAY29udGVudDtcclxuXHR9XHJcbn1cclxuQG1peGluIG5vc2VsZWN0KCkge1xyXG5cdC13ZWJraXQtdG91Y2gtY2FsbG91dDogbm9uZTtcclxuXHQvKiBpT1MgU2FmYXJpICovXHJcblx0LXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuXHQvKiBTYWZhcmkgKi9cclxuXHQta2h0bWwtdXNlci1zZWxlY3Q6IG5vbmU7XHJcblx0LyogS29ucXVlcm9yIEhUTUwgKi9cclxuXHQtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG5cdC8qIEZpcmVmb3ggKi9cclxuXHQtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcblx0LyogSW50ZXJuZXQgRXhwbG9yZXIvRWRnZSAqL1xyXG5cdHVzZXItc2VsZWN0OiBub25lO1xyXG5cdC8qIE5vbi1wcmVmaXhlZCB2ZXJzaW9uLCBjdXJyZW50bHlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1cHBvcnRlZCBieSBDaHJvbWUgYW5kIE9wZXJhICovXHJcbn1cclxuXHJcblxyXG5AbWl4aW4gZGVmQm94U2hhZG93KCkge1xyXG5cdGJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcclxufVxyXG5cclxuXHJcbkBtaXhpbiBkZWZCb3hTaGFkb3dTdHJvbmcoKSB7XHJcblx0Ym94LXNoYWRvdzogMCAzcHggM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjUpLCAwIDRweCAycHggLTNweCByZ2JhKDAsIDAsIDAsIDAuNSksIDAgMnB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC40KTtcclxufVxyXG5cclxuQG1peGluIGRlZkJveFNoYWRvd0luc2V0KCkge1xyXG5cdGJveC1zaGFkb3c6IGluc2V0IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIGluc2V0IDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xMiksIGluc2V0IDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcclxufVxyXG5cclxuQG1peGluIGRlZkJvcmRlcigpIHtcclxuXHRib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTI1KTtcclxufVxyXG5cclxuQG1peGluIGRlZkhSKCkge1xyXG5cdGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbn1cclxuXHJcblxyXG5AbWl4aW4gYm94U2hhZG93TGlnaHQoKSB7XHJcblx0Ym94LXNoYWRvdzogMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjEyKSwgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG59XHJcblxyXG5cclxuQG1peGluIGJveFNoYWRvdygpIHtcclxuXHRib3gtc2hhZG93OiAwIDRweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMjgpLCAwIDZweCAycHggLTRweCByZ2JhKDAsIDAsIDAsIDAuMjgpLCAwIDJweCAxMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG59XHJcblxyXG5AbWl4aW4gYm94U2hhZG93U3Ryb25nKCkge1xyXG5cdGJveC1zaGFkb3c6IDAgOHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yOCksIDAgMTJweCA0cHggLThweCByZ2JhKDAsIDAsIDAsIDAuMjgpLCAwIDRweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG59XHJcblxyXG5AbWl4aW4gYm94U2hhZG93SW5zZXRMaWdodCgpIHtcclxuXHRib3gtc2hhZG93OiBpbnNldCAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCBpbnNldCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTIpLCBpbnNldCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbn1cclxuXHJcbkBtaXhpbiBib3hTaGFkb3dJbnNldCgpIHtcclxuXHRib3gtc2hhZG93OiBpbnNldCAwIDRweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuMjgpLCBpbnNldCAwIDZweCAycHggLTRweCByZ2JhKDAsIDAsIDAsIDAuMjQpLCBpbnNldCAwIDJweCAxMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG59XHJcblxyXG5AbWl4aW4gYm94U2hhZG93SW5zZXRTdHJvbmcoKSB7XHJcblx0Ym94LXNoYWRvdzogaW5zZXQgMCA4cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjI4KSwgaW5zZXQgMCAxMnB4IDRweCAtNHB4IHJnYmEoMCwgMCwgMCwgMC4yNCksIGluc2V0IDAgNHB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbn1cclxuXHJcbiJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotificationComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'notification',
            templateUrl: './notification.component.html',
            styleUrls: ['./notification.component.scss']
          }]
        }], function () {
          return [];
        }, {
          message: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          closeMessage: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }]
        });
      })();
      /***/

    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! C:\xampp\htdocs\typing\src\main.ts */
      "zUnb");
      /***/
    },

    /***/
    "0/Ww":
    /*!*****************************************************!*\
      !*** ./src/app/services/lesson-generate.service.ts ***!
      \*****************************************************/

    /*! exports provided: LessonGenerateService */

    /***/
    function Ww(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LessonGenerateService", function () {
        return LessonGenerateService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _setting_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./setting.service */
      "KQZp");
      /* harmony import */


      var _word_generate_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./word-generate.service */
      "kKNg");

      var LessonGenerateService = /*#__PURE__*/function () {
        function LessonGenerateService(_wg, _settings) {
          _classCallCheck(this, LessonGenerateService);

          this._wg = _wg;
          this._settings = _settings;
        }

        _createClass(LessonGenerateService, [{
          key: "get",
          value: function get() {
            var text = '';
            var s = this._settings.get;

            while (text.length + s.minWordLength + 1 < s.lessonLength) {
              var freeLength = s.lessonLength - text.length;
              var maxWordLength = Math.min(freeLength, s.maxWordLength);
              var wordLength = Math.floor(Math.random() * (maxWordLength - s.minWordLength + 1) + s.minWordLength);
              text += this._wg.get(wordLength) + ' ';
            }

            text = text.slice(0, -1);
            return text;
          }
        }]);

        return LessonGenerateService;
      }();

      LessonGenerateService.ɵfac = function LessonGenerateService_Factory(t) {
        return new (t || LessonGenerateService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_word_generate_service__WEBPACK_IMPORTED_MODULE_2__["WordGenerateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_setting_service__WEBPACK_IMPORTED_MODULE_1__["SettingService"]));
      };

      LessonGenerateService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: LessonGenerateService,
        factory: LessonGenerateService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LessonGenerateService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _word_generate_service__WEBPACK_IMPORTED_MODULE_2__["WordGenerateService"]
          }, {
            type: _setting_service__WEBPACK_IMPORTED_MODULE_1__["SettingService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "1VCC":
    /*!***********************************************************!*\
      !*** ./src/app/components/progress/progress.component.ts ***!
      \***********************************************************/

    /*! exports provided: Progress, ProgressComponent */

    /***/
    function VCC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Progress", function () {
        return Progress;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProgressComponent", function () {
        return ProgressComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = function _c0(a0) {
        return {
          "width.%": a0
        };
      };

      function ProgressComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 3);
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c0, 100 * ctx_r0.progress.value / ctx_r0.progress.max));
        }
      }

      function ProgressComponent_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r1.progress.label);
        }
      }

      var Progress = function Progress() {
        _classCallCheck(this, Progress);
      };

      var ProgressComponent = /*#__PURE__*/function () {
        function ProgressComponent() {
          _classCallCheck(this, ProgressComponent);

          this.size = 'default';
        }

        _createClass(ProgressComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ProgressComponent;
      }();

      ProgressComponent.ɵfac = function ProgressComponent_Factory(t) {
        return new (t || ProgressComponent)();
      };

      ProgressComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ProgressComponent,
        selectors: [["app-progress"]],
        inputs: {
          size: "size",
          progress: "progress"
        },
        decls: 3,
        vars: 4,
        consts: [[1, "progress"], ["class", "bar", 3, "ngStyle", 4, "ngIf"], ["class", "label", 4, "ngIf"], [1, "bar", 3, "ngStyle"], [1, "label"]],
        template: function ProgressComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ProgressComponent_div_1_Template, 1, 3, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ProgressComponent_div_2_Template, 2, 1, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx.size);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.progress);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.progress == null ? null : ctx.progress.label);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgStyle"]],
        styles: [".progress[_ngcontent-%COMP%] {\n  height: 26px;\n  background-color: #444;\n  position: relative;\n  overflow: hidden;\n}\n.progress.big[_ngcontent-%COMP%] {\n  height: 100px;\n}\n.progress.big[_ngcontent-%COMP%]   .label[_ngcontent-%COMP%] {\n  line-height: 100px;\n  font-size: 2rem;\n}\n.progress.big[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%] {\n  transition: 3s;\n}\n.progress[_ngcontent-%COMP%]   .bar[_ngcontent-%COMP%] {\n  height: 100%;\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 80%;\n  z-index: 1;\n  background-color: #4f00c4;\n  box-shadow: 0 0 8px 2px rgba(0, 0, 0, 0.8);\n  transition: 1s;\n}\n.progress[_ngcontent-%COMP%]   .label[_ngcontent-%COMP%] {\n  text-align: center;\n  line-height: 26px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  z-index: 2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9zY3NzL192YXJpYWJsZXMuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50cy9wcm9ncmVzcy9wcm9ncmVzcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBQTtBQ0VBO0VBQ0UsWUFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUFGO0FBQ0U7RUFDRSxhQUFBO0FBQ0o7QUFBSTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtBQUVOO0FBQUk7RUFDRSxjQUFBO0FBRU47QUFDRTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EseUJEYkM7RUNjRCwwQ0FBQTtFQUNBLGNBQUE7QUFDSjtBQUNFO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9wcm9ncmVzcy9wcm9ncmVzcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGJyZWFrcG9pbnRzICovXHJcbiR4czogMDtcclxuJHNtOiA2MDBweDtcclxuJG1kOiA5NjBweDtcclxuJGxnOiAxMjgwcHg7XHJcbiR4bDogMTkyMHB4O1xyXG5cclxuXHJcblxyXG5cclxuJGMxOiAjNTgzNGViO1xyXG4kYzM6ICMwMGY3ZmY7XHJcbiRjMjogIzRmMDBjNDtcclxuJGRhcmtncmV5OiAjMTgxODE4O1xyXG4kcmVkOiAjYjEwMTAxO1xyXG5cclxuIiwiQGltcG9ydCAndmFyaWFibGVzJztcclxuXHJcbi5wcm9ncmVzcyB7XHJcbiAgaGVpZ2h0OiAyNnB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0NDQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgJi5iaWcge1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIC5sYWJlbCB7XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiAxMDBweDtcclxuICAgICAgZm9udC1zaXplOiAycmVtO1xyXG4gICAgfVxyXG4gICAgLmJhciB7XHJcbiAgICAgIHRyYW5zaXRpb246IDNzO1xyXG4gICAgfVxyXG4gIH1cclxuICAuYmFyIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjMjtcclxuICAgIGJveC1zaGFkb3c6IDAgMCA4cHggMnB4IHJnYmEoIzAwMCwgLjgpO1xyXG4gICAgdHJhbnNpdGlvbjogMXM7XHJcbiAgfVxyXG4gIC5sYWJlbCB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBsaW5lLWhlaWdodDogMjZweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHotaW5kZXg6IDI7XHJcbiAgfVxyXG59Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProgressComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-progress',
            templateUrl: './progress.component.html',
            styleUrls: ['./progress.component.scss']
          }]
        }], function () {
          return [];
        }, {
          size: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          progress: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "1XqU":
    /*!***********************************!*\
      !*** ./src/app/models/phonset.ts ***!
      \***********************************/

    /*! exports provided: PhonSet */

    /***/
    function XqU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PhonSet", function () {
        return PhonSet;
      });
      /* harmony import */


      var _score__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./score */
      "QAQN");

      var PhonSet = function PhonSet(name, vocales, consonants) {
        var level = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

        _classCallCheck(this, PhonSet);

        this.exp = 0;
        this.name = name;
        this.vocales = vocales;
        this.consonants = consonants;
        this.level = level;
        this.score = [];

        var _iterator = _createForOfIteratorHelper(vocales),
            _step;

        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var v = _step.value;
            this.score.push(new _score__WEBPACK_IMPORTED_MODULE_0__["Score"]({
              sign: v.sign,
              phonType: 'vocale'
            }));
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }

        var _iterator2 = _createForOfIteratorHelper(consonants),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var c = _step2.value;
            this.score.push(new _score__WEBPACK_IMPORTED_MODULE_0__["Score"]({
              sign: c.sign,
              phonType: 'consonant'
            }));
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
      };
      /***/

    },

    /***/
    "2MiI":
    /*!*******************************************************!*\
      !*** ./src/app/components/header/header.component.ts ***!
      \*******************************************************/

    /*! exports provided: HeaderComponent */

    /***/
    function MiI(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
        return HeaderComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var HeaderComponent = /*#__PURE__*/function () {
        function HeaderComponent() {
          _classCallCheck(this, HeaderComponent);
        }

        _createClass(HeaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return HeaderComponent;
      }();

      HeaderComponent.ɵfac = function HeaderComponent_Factory(t) {
        return new (t || HeaderComponent)();
      };

      HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: HeaderComponent,
        selectors: [["app-header"]],
        decls: 0,
        vars: 0,
        template: function HeaderComponent_Template(rf, ctx) {},
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeaderComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "CfCB":
    /*!*******************************!*\
      !*** ./src/app/data/phons.ts ***!
      \*******************************/

    /*! exports provided: vocales, consonants */

    /***/
    function CfCB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "vocales", function () {
        return vocales;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "consonants", function () {
        return consonants;
      });
      /* harmony import */


      var _models_phon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../models/phon */
      "GPe3");

      var vocales = [new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('a'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('e'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('i'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('o'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('u'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ä'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ö'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ü'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ae', 2), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ai', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ao', 2), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('au', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ea', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ei', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('eo', 6), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('eu', 6), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ia', 6), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ie', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('io', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('iu', 6), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('oa', 6), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('oe', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('oi', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ou', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ua', 6), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ue', 4), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ui', 7), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('uo', 6)];
      var consonants = [new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('b'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bb', 2), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bc', 3), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bd', 3), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bf', 3), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bg', 5), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('br'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('by'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('bz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('c'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ch'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ck'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ct'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('cz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('d'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('db'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('df'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ds'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('dz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('f'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ff'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ft'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('fz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('g'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('gz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('h'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ht'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('hz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('j'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('js'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('jz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('k'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('km'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ks'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ky'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('kz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('l'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ld'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ll'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ln'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ls'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ly'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('lz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('m'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('md'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ml'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ms'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('my'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('mz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('n'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ng'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('np'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ns'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ny'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('nz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('p'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ph'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ps'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('px'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('py'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('pz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('q'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ql'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('qz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('r'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ry'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('rz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('s'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ss'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('st'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('sz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('t'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('td'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('th'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ts'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ty'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('tz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('v'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('vz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('w'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ws'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ww'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('wz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('x'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('xz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('y'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ym'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('ys'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('yz'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('z'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zb'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zc'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zd'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zf'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zg'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zh'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zj'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zk'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zl'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zm'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zn'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zp'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zq'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zr'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zs'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zt'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zv'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zw'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zx'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zy'), new _models_phon__WEBPACK_IMPORTED_MODULE_0__["Phon"]('zz')];
      /***/
    },

    /***/
    "GPe3":
    /*!********************************!*\
      !*** ./src/app/models/phon.ts ***!
      \********************************/

    /*! exports provided: Phon */

    /***/
    function GPe3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Phon", function () {
        return Phon;
      });

      var Phon = function Phon(sign) {
        var frequency = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : .8;
        var neverAtTheBegin = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        var neverAtTheEnd = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
        var noCap = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

        _classCallCheck(this, Phon);

        this.neverAtTheBegin = false;
        this.neverAtTheEnd = false;
        this.noCap = false;
        this.sign = sign;
        this.frequency = frequency;
        this.neverAtTheBegin = neverAtTheBegin;
        this.neverAtTheEnd = neverAtTheEnd;
        this.noCap = noCap;
      };
      /***/

    },

    /***/
    "IBgE":
    /*!**************************************************************!*\
      !*** ./src/app/modules/notification/notification.service.ts ***!
      \**************************************************************/

    /*! exports provided: NotificationService */

    /***/
    function IBgE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationService", function () {
        return NotificationService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");

      var NotificationService = /*#__PURE__*/function () {
        function NotificationService() {
          _classCallCheck(this, NotificationService);

          this.recentNotificationId = 0;
          this.notifications = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        }

        _createClass(NotificationService, [{
          key: "show",
          value: function show(message) {
            var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'info';
            var duration = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 5000;
            var icon = arguments.length > 3 ? arguments[3] : undefined;
            this.notifications.next({
              id: this.recentNotificationId++,
              message: message,
              icon: icon,
              type: type,
              duration: duration
            });
          }
        }]);

        return NotificationService;
      }();

      NotificationService.ɵfac = function NotificationService_Factory(t) {
        return new (t || NotificationService)();
      };

      NotificationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: NotificationService,
        factory: NotificationService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotificationService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "JiyW":
    /*!*******************************************************!*\
      !*** ./src/app/components/lesson/lesson.component.ts ***!
      \*******************************************************/

    /*! exports provided: LessonComponent */

    /***/
    function JiyW(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LessonComponent", function () {
        return LessonComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var src_app_data_level_strategy__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/data/level.strategy */
      "ygmV");
      /* harmony import */


      var src_app_services_lesson_generate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/lesson-generate.service */
      "0/Ww");
      /* harmony import */


      var src_app_services_phon_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/services/phon.service */
      "vgeL");
      /* harmony import */


      var src_app_services_reward_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/services/reward.service */
      "rk0U");
      /* harmony import */


      var src_app_services_setting_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/services/setting.service */
      "KQZp");
      /* harmony import */


      var src_app_services_text_reader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/text-reader.service */
      "hDew");
      /* harmony import */


      var _progress_progress_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../progress/progress.component */
      "1VCC");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _working_on_working_on_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./working-on/working-on.component */
      "f5OE");

      var _c0 = ["letter"];

      function LessonComponent_div_4_Template(rf, ctx) {
        if (rf & 1) {
          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LessonComponent_div_4_Template_div_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

            var mode_r4 = ctx.$implicit;

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r5.changeTrainingMode(mode_r4);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var mode_r4 = ctx.$implicit;

          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("active", ctx_r0.trainingMode === mode_r4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", mode_r4, " ");
        }
      }

      function LessonComponent_app_working_on_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-working-on", 9);
        }

        if (rf & 2) {
          var score_r7 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("score", score_r7);
        }
      }

      function LessonComponent_app_working_on_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-working-on", 9);
        }

        if (rf & 2) {
          var score_r8 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("score", score_r8);
        }
      }

      function LessonComponent_div_9_span_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 12, 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var letter_r11 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("written", letter_r11.written)("missspelled", letter_r11.missspelled);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", letter_r11["char"], " ");
        }
      }

      function LessonComponent_div_9_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, LessonComponent_div_9_span_1_Template, 3, 5, "span", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var word_r9 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", word_r9);
        }
      }

      var _c1 = function _c1(a0, a1, a2) {
        return {
          value: a0,
          max: a1,
          label: a2
        };
      };

      var WrittenLetter = function WrittenLetter() {
        _classCallCheck(this, WrittenLetter);
      };

      var LessonComponent = /*#__PURE__*/function () {
        function LessonComponent(_lesson, _phonService, _settingService, _tr, _reward) {
          _classCallCheck(this, LessonComponent);

          this._lesson = _lesson;
          this._phonService = _phonService;
          this._settingService = _settingService;
          this._tr = _tr;
          this._reward = _reward;
          this.writtenText = [[]];
          this.writtenWords = 0;
          this.trainingMode = src_app_services_setting_service__WEBPACK_IMPORTED_MODULE_6__["ETrainingMode"].training;
          this.textCursorNewPos = false;
          this._alive = true;
          this.heroName = 'Hero';
          this.mainLevel = 0;
          this.mainExp = 0;
          this.mainMaxExp = 0;
          this.shakeTextcursor = false;
          this.trainingModes = Object.values(src_app_services_setting_service__WEBPACK_IMPORTED_MODULE_6__["ETrainingMode"]);
          this.badScoreForThisText = [];
          this.textcursorPos = {
            top: '0',
            left: '0'
          };
        }

        _createClass(LessonComponent, [{
          key: "onKeydownHandler",
          value: function onKeydownHandler(evt) {
            this.checkInput(evt.key);
          }
        }, {
          key: "lastWord",
          get: function get() {
            var words = this.writtenText.filter(function (w) {
              var _a;

              return (_a = w[0]) === null || _a === void 0 ? void 0 : _a.written;
            });
            return words[words.length - 1];
          }
        }, {
          key: "nextLetter",
          get: function get() {
            var _iterator3 = _createForOfIteratorHelper(this.writtenText),
                _step3;

            try {
              for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                var w = _step3.value;

                var _iterator4 = _createForOfIteratorHelper(w),
                    _step4;

                try {
                  for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
                    var l = _step4.value;

                    if (!l.written) {
                      return l;
                    }
                  }
                } catch (err) {
                  _iterator4.e(err);
                } finally {
                  _iterator4.f();
                }
              }
            } catch (err) {
              _iterator3.e(err);
            } finally {
              _iterator3.f();
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.getText();

            this._phonService.curSet.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["takeWhile"])(function () {
              return _this._alive;
            })).subscribe(function (set) {
              _this.heroName = set.name;
              _this.mainLevel = set.level;
              _this.mainExp = set.exp;
              _this.mainMaxExp = src_app_data_level_strategy__WEBPACK_IMPORTED_MODULE_2__["LevelStrategy"].maxMainExp(set.level);
              console.log('this.mainExp', _this.mainExp);
              console.log('this.mainMaxExp', _this.mainMaxExp);
            });

            this._phonService.almostLvlUp.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["takeWhile"])(function () {
              return _this._alive;
            })).subscribe(function (almostLvlUp) {
              _this.almostLvlUp = almostLvlUp.filter(function (sc) {
                return !_this.badScoreForThisText.map(function (s) {
                  return s.sign;
                }).includes(sc.sign);
              });
            });

            this._settingService.settings.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["takeWhile"])(function () {
              return _this._alive;
            })).subscribe(function (set) {
              _this.trainingMode = set.trainingMode;
            });
          }
        }, {
          key: "checkInput",
          value: function checkInput(input) {
            var _this2 = this;

            var _a;

            if (input === ' ') {
              this.wordStartTime = Date.now();
            }

            if (!this.textStartTime) {
              this.textStartTime = Date.now();
              this.wordStartTime = Date.now();
            }

            if (input === ((_a = this.nextLetter) === null || _a === void 0 ? void 0 : _a["char"])) {
              this.nextLetterIsDone();

              if (!this.nextLetter || this.nextLetter["char"] === ' ') {
                this.nextWordIsDone();

                if (!this.nextLetter) {
                  this.textCompleted();
                }
              }

              this.setTextCursor();
            } else if (this.nextLetter) {
              this.nextLetter.missspelled++;
              this.shakeTextcursor = true;
              setTimeout(function () {
                _this2.shakeTextcursor = false;
              }, 100);
            }
          }
        }, {
          key: "nextLetterIsDone",
          value: function nextLetterIsDone() {
            this.nextLetter.written = true;
            this.writtenWords++;
          }
        }, {
          key: "nextWordIsDone",
          value: function nextWordIsDone() {
            var word = this.lastWord.reduce(function (prev, w) {
              return prev + w["char"];
            }, '');
            var duration = Date.now() - this.wordStartTime;
            var missspelled = this.lastWord.reduce(function (prev, w) {
              return prev + w.missspelled;
            }, 0);

            this._reward.getReward(word, duration, missspelled);
          }
        }, {
          key: "textCompleted",
          value: function textCompleted() {
            this._phonService.save();

            console.log('textCompleted');
            this.getText();
          }
        }, {
          key: "getText",
          value: function getText() {
            var _this3 = this;

            var text = this._lesson.get();

            this.badScoreForThisText = [].concat(_toConsumableArray(this._phonService.getBadScore('consonant')), _toConsumableArray(this._phonService.getBadScore('vocale')));
            this.writtenText = [];
            var letters = [];

            for (var i = 0; i < text.length; i++) {
              letters.push({
                "char": text[i],
                written: false,
                missspelled: 0
              });

              if (text[i] === ' ') {
                this.writtenText.push(letters);
                letters = [];
              }
            }

            if (letters.length > 0) {
              this.writtenText.push(letters);
            }

            this.textStartTime = null;
            setTimeout(function () {
              _this3.writtenWords = 0;

              _this3.setTextCursor();
            });
          }
        }, {
          key: "setTextCursor",
          value: function setTextCursor() {
            var lastWrittenLetter = this.letterElements.toArray()[this.writtenWords];
            if (!lastWrittenLetter) return;
            var pos = lastWrittenLetter.nativeElement.getBoundingClientRect();
            this.textCursorNewPos = pos.top + 'px' !== this.textcursorPos.top;
            this.textcursorPos.top = pos.top + 'px';
            this.textcursorPos.left = pos.left + 'px';
          }
        }, {
          key: "changeTrainingMode",
          value: function changeTrainingMode(mode) {
            this._settingService.changeTrainingMode(mode);

            this.getText();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this._alive = false;
          }
        }]);

        return LessonComponent;
      }();

      LessonComponent.ɵfac = function LessonComponent_Factory(t) {
        return new (t || LessonComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_lesson_generate_service__WEBPACK_IMPORTED_MODULE_3__["LessonGenerateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_phon_service__WEBPACK_IMPORTED_MODULE_4__["PhonService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_setting_service__WEBPACK_IMPORTED_MODULE_6__["SettingService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_text_reader_service__WEBPACK_IMPORTED_MODULE_7__["TextReaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_reward_service__WEBPACK_IMPORTED_MODULE_5__["RewardService"]));
      };

      LessonComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: LessonComponent,
        selectors: [["app-lession"]],
        viewQuery: function LessonComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.letterElements = _t);
          }
        },
        hostBindings: function LessonComponent_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keydown", function LessonComponent_keydown_HostBindingHandler($event) {
              return ctx.onKeydownHandler($event);
            }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveDocument"]);
          }
        },
        decls: 12,
        vars: 15,
        consts: [["size", "big", 3, "progress"], [1, "mode-select"], ["class", "mode", 3, "active", "click", 4, "ngFor", "ngForOf"], [1, "working-on"], [3, "score", 4, "ngFor", "ngForOf"], [1, "text"], ["class", "word", 4, "ngFor", "ngForOf"], [1, "textcursor", 3, "ngStyle"], [1, "mode", 3, "click"], [3, "score"], [1, "word"], ["class", "letter", 3, "written", "missspelled", 4, "ngFor", "ngForOf"], [1, "letter"], ["letter", ""]],
        template: function LessonComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-progress", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " mode: ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, LessonComponent_div_4_Template, 2, 3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, LessonComponent_app_working_on_6_Template, 1, 1, "app-working-on", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, LessonComponent_app_working_on_7_Template, 1, 1, "app-working-on", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, LessonComponent_div_9_Template, 2, 1, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "|");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("progress", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](11, _c1, ctx.mainExp, ctx.mainMaxExp, ctx.heroName + " Level " + (ctx.mainLevel + 1)));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("\n", ctx.mainMaxExp, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.trainingModes);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.badScoreForThisText);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.almostLvlUp);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.writtenText);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("shake", ctx.shakeTextcursor)("no-transition", ctx.textCursorNewPos);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", ctx.textcursorPos);
          }
        },
        directives: [_progress_progress_component__WEBPACK_IMPORTED_MODULE_8__["ProgressComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgStyle"], _working_on_working_on_component__WEBPACK_IMPORTED_MODULE_10__["WorkingOnComponent"]],
        styles: [".mode-select[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n.mode-select[_ngcontent-%COMP%]   .mode[_ngcontent-%COMP%] {\n  padding: 0.25rem 0.5rem;\n  border-radius: 8px;\n  transition: 0.5s;\n  cursor: pointer;\n}\n.mode-select[_ngcontent-%COMP%]   .mode[_ngcontent-%COMP%]:hover {\n  color: #fff;\n  background-color: rgba(255, 255, 255, 0.2);\n}\n.mode-select[_ngcontent-%COMP%]   .mode.active[_ngcontent-%COMP%] {\n  color: #fff;\n}\n.text[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n  margin: 2rem auto;\n  padding: 2rem;\n  border: 1px solid #000;\n  display: flex;\n  align-items: center;\n  flex-wrap: wrap;\n  max-width: 1280px;\n}\n.text[_ngcontent-%COMP%]   .word[_ngcontent-%COMP%] {\n  margin-right: 0.75rem;\n  display: flex;\n}\n.text[_ngcontent-%COMP%]   .letter[_ngcontent-%COMP%] {\n  display: inline-block;\n}\n.text[_ngcontent-%COMP%]   .letter.written[_ngcontent-%COMP%] {\n  color: #5834eb;\n}\n.text[_ngcontent-%COMP%]   .letter.missspelled[_ngcontent-%COMP%] {\n  color: #b10101;\n}\n.text[_ngcontent-%COMP%]   .textcursor[_ngcontent-%COMP%] {\n  position: absolute;\n  transform: translate(-50%, -10%);\n  color: #eee;\n  font-size: 1.75rem;\n  transition: left 0.2s, top 3s;\n}\n.text[_ngcontent-%COMP%]   .textcursor.no-transition[_ngcontent-%COMP%] {\n  transition: 0s;\n}\n.text[_ngcontent-%COMP%]   .textcursor.shake[_ngcontent-%COMP%] {\n  -webkit-animation: shaketextcursor 0.1s infinite;\n          animation: shaketextcursor 0.1s infinite;\n}\n.working-on[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  gap: 1rem;\n}\n@-webkit-keyframes shaketextcursor {\n  0% {\n    margin-top: 0;\n  }\n  33% {\n    margin-top: -5px;\n  }\n  66% {\n    margin-top: 5px;\n  }\n  100% {\n    margin-top: 0;\n  }\n}\n@keyframes shaketextcursor {\n  0% {\n    margin-top: 0;\n  }\n  33% {\n    margin-top: -5px;\n  }\n  66% {\n    margin-top: 5px;\n  }\n  100% {\n    margin-top: 0;\n  }\n}\n@-webkit-keyframes blinktextcursor {\n  0% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0.2;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n@keyframes blinktextcursor {\n  0% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0.2;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9zY3NzL192YXJpYWJsZXMuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50cy9sZXNzb24vbGVzc29uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFBO0FDRUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUFBRjtBQUNFO0VBQ0UsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQUNKO0FBQ0k7RUFDRSxXQUFBO0VBQ0EsMENBQUE7QUFDTjtBQUNJO0VBQ0UsV0FBQTtBQUNOO0FBSUE7RUFDRSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCRHpCRztBQ3dCTDtBQUVFO0VBQ0UscUJBQUE7RUFFQSxhQUFBO0FBREo7QUFHRTtFQUNFLHFCQUFBO0FBREo7QUFFSTtFQUNFLGNENUJEO0FDNEJMO0FBRUk7RUFDRSxjRDNCQTtBQzJCTjtBQUdFO0VBQ0Usa0JBQUE7RUFDQSxnQ0FBQTtFQUNBLFdBQUE7RUFFQSxrQkFBQTtFQUNBLDZCQUFBO0FBRko7QUFHSTtFQUNFLGNBQUE7QUFETjtBQUdJO0VBQ0UsZ0RBQUE7VUFBQSx3Q0FBQTtBQUROO0FBTUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFNBQUE7QUFIRjtBQU1BO0VBQ0U7SUFDRSxhQUFBO0VBSEY7RUFLQTtJQUNFLGdCQUFBO0VBSEY7RUFLQTtJQUNFLGVBQUE7RUFIRjtFQUtBO0lBQ0UsYUFBQTtFQUhGO0FBQ0Y7QUFUQTtFQUNFO0lBQ0UsYUFBQTtFQUhGO0VBS0E7SUFDRSxnQkFBQTtFQUhGO0VBS0E7SUFDRSxlQUFBO0VBSEY7RUFLQTtJQUNFLGFBQUE7RUFIRjtBQUNGO0FBTUE7RUFDRTtJQUNFLFVBQUE7RUFKRjtFQU1BO0lBQ0UsWUFBQTtFQUpGO0VBTUE7SUFDRSxVQUFBO0VBSkY7QUFDRjtBQUxBO0VBQ0U7SUFDRSxVQUFBO0VBSkY7RUFNQTtJQUNFLFlBQUE7RUFKRjtFQU1BO0lBQ0UsVUFBQTtFQUpGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2xlc3Nvbi9sZXNzb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBicmVha3BvaW50cyAqL1xyXG4keHM6IDA7XHJcbiRzbTogNjAwcHg7XHJcbiRtZDogOTYwcHg7XHJcbiRsZzogMTI4MHB4O1xyXG4keGw6IDE5MjBweDtcclxuXHJcblxyXG5cclxuXHJcbiRjMTogIzU4MzRlYjtcclxuJGMzOiAjMDBmN2ZmO1xyXG4kYzI6ICM0ZjAwYzQ7XHJcbiRkYXJrZ3JleTogIzE4MTgxODtcclxuJHJlZDogI2IxMDEwMTtcclxuXHJcbiIsIkBpbXBvcnQgJ3ZhcmlhYmxlcyc7XHJcblxyXG4ubW9kZS1zZWxlY3Qge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAubW9kZSB7XHJcbiAgICBwYWRkaW5nOiAuMjVyZW0gLjVyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICB0cmFuc2l0aW9uOiAuNXM7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBcclxuICAgICY6aG92ZXIge1xyXG4gICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgjZmZmLCAuMik7XHJcbiAgICB9XHJcbiAgICAmLmFjdGl2ZSB7XHJcbiAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLnRleHQge1xyXG4gIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gIG1hcmdpbjogMnJlbSBhdXRvO1xyXG4gIHBhZGRpbmc6IDJyZW07XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIG1heC13aWR0aDogJGxnO1xyXG4gIC53b3JkIHtcclxuICAgIG1hcmdpbi1yaWdodDogLjc1cmVtO1xyXG5cclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgfVxyXG4gIC5sZXR0ZXIge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgJi53cml0dGVuIHtcclxuICAgICAgY29sb3I6ICRjMTtcclxuICAgIH1cclxuICAgICYubWlzc3NwZWxsZWQge1xyXG4gICAgICBjb2xvcjogJHJlZDtcclxuICAgIH1cclxuICB9XHJcbiAgLnRleHRjdXJzb3Ige1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTEwJSk7XHJcbiAgICBjb2xvcjogI2VlZTtcclxuICAgIC8vIGFuaW1hdGlvbjogYmxpbmt0ZXh0Y3Vyc29yIDJzIGluZmluaXRlO1xyXG4gICAgZm9udC1zaXplOiAxLjc1cmVtO1xyXG4gICAgdHJhbnNpdGlvbjogbGVmdCAuMnMsIHRvcCAzcztcclxuICAgICYubm8tdHJhbnNpdGlvbiB7XHJcbiAgICAgIHRyYW5zaXRpb246IDBzO1xyXG4gICAgfVxyXG4gICAgJi5zaGFrZSB7XHJcbiAgICAgIGFuaW1hdGlvbjogc2hha2V0ZXh0Y3Vyc29yIC4xcyBpbmZpbml0ZTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi53b3JraW5nLW9uIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZ2FwOiAxcmVtO1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHNoYWtldGV4dGN1cnNvciB7XHJcbiAgMCUge1xyXG4gICAgbWFyZ2luLXRvcDogMDtcclxuICB9XHJcbiAgMzMlIHtcclxuICAgIG1hcmdpbi10b3A6IC01cHg7XHJcbiAgfVxyXG4gIDY2JSB7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgfVxyXG4gIDEwMCUge1xyXG4gICAgbWFyZ2luLXRvcDogMDtcclxuICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgYmxpbmt0ZXh0Y3Vyc29yIHtcclxuICAwJSB7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gIH1cclxuICA1MCUge1xyXG4gICAgb3BhY2l0eTogMC4yO1xyXG4gIH1cclxuICAxMDAlIHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgfVxyXG59Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LessonComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-lession',
            templateUrl: './lesson.component.html',
            styleUrls: ['./lesson.component.scss']
          }]
        }], function () {
          return [{
            type: src_app_services_lesson_generate_service__WEBPACK_IMPORTED_MODULE_3__["LessonGenerateService"]
          }, {
            type: src_app_services_phon_service__WEBPACK_IMPORTED_MODULE_4__["PhonService"]
          }, {
            type: src_app_services_setting_service__WEBPACK_IMPORTED_MODULE_6__["SettingService"]
          }, {
            type: src_app_services_text_reader_service__WEBPACK_IMPORTED_MODULE_7__["TextReaderService"]
          }, {
            type: src_app_services_reward_service__WEBPACK_IMPORTED_MODULE_5__["RewardService"]
          }];
        }, {
          letterElements: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChildren"],
            args: ['letter']
          }],
          onKeydownHandler: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['document:keydown', ['$event']]
          }]
        });
      })();
      /***/

    },

    /***/
    "KQZp":
    /*!*********************************************!*\
      !*** ./src/app/services/setting.service.ts ***!
      \*********************************************/

    /*! exports provided: ETrainingMode, Settings, SettingService */

    /***/
    function KQZp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ETrainingMode", function () {
        return ETrainingMode;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Settings", function () {
        return Settings;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SettingService", function () {
        return SettingService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./storage.service */
      "n90K");

      var ETrainingMode;

      (function (ETrainingMode) {
        ETrainingMode["random"] = "random";
        ETrainingMode["mixed"] = "mixed";
        ETrainingMode["training"] = "training";
        ETrainingMode["focus"] = "focus";
        ETrainingMode["hardFocus"] = "hardFocus";
      })(ETrainingMode || (ETrainingMode = {}));

      var Settings = function Settings(obj) {
        _classCallCheck(this, Settings);

        var _a, _b, _c, _d;

        this.minWordLength = (_a = obj.minWordLength) !== null && _a !== void 0 ? _a : 3;
        this.maxWordLength = (_b = obj.maxWordLength) !== null && _b !== void 0 ? _b : 8;
        this.lessonLength = (_c = obj.lessonLength) !== null && _c !== void 0 ? _c : 200;
        this.trainingMode = (_d = obj.trainingMode) !== null && _d !== void 0 ? _d : ETrainingMode.training;
      };

      var SettingService = /*#__PURE__*/function () {
        function SettingService(_storageService) {
          _classCallCheck(this, SettingService);

          this._storageService = _storageService;
          this.settings = new rxjs__WEBPACK_IMPORTED_MODULE_1__["ReplaySubject"]();
          this.load();
          this.settings.next(this._settings);
        }

        _createClass(SettingService, [{
          key: "get",
          get: function get() {
            return this._settings;
          }
        }, {
          key: "save",
          value: function save() {
            this._storageService.save('typingsettings', this._settings);

            this.settings.next(this._settings);
          }
        }, {
          key: "load",
          value: function load() {
            // this._settings = this._storageService.get<Settings>('typingsettings');
            if (!this._settings) {
              this._settings = new Settings({});
            }
          }
        }, {
          key: "changeTrainingMode",
          value: function changeTrainingMode(mode) {
            this._settings.trainingMode = mode;
            this.save();
          }
        }]);

        return SettingService;
      }();

      SettingService.ɵfac = function SettingService_Factory(t) {
        return new (t || SettingService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_storage_service__WEBPACK_IMPORTED_MODULE_2__["StorageService"]));
      };

      SettingService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: SettingService,
        factory: SettingService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SettingService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _storage_service__WEBPACK_IMPORTED_MODULE_2__["StorageService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "LmEr":
    /*!*******************************************************!*\
      !*** ./src/app/components/footer/footer.component.ts ***!
      \*******************************************************/

    /*! exports provided: FooterComponent */

    /***/
    function LmEr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FooterComponent", function () {
        return FooterComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var FooterComponent = /*#__PURE__*/function () {
        function FooterComponent() {
          _classCallCheck(this, FooterComponent);
        }

        _createClass(FooterComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FooterComponent;
      }();

      FooterComponent.ɵfac = function FooterComponent_Factory(t) {
        return new (t || FooterComponent)();
      };

      FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: FooterComponent,
        selectors: [["app-footer"]],
        decls: 5,
        vars: 2,
        consts: [[1, "footer"], ["routerLink", "lesson", 3, "routerLinkActive"], ["routerLink", "set", 3, "routerLinkActive"]],
        template: function FooterComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " start ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Sets ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLinkActive", "active");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLinkActive", "active");
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"]],
        styles: [".footer[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.footer[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  margin: 0 0.5rem;\n  padding: 0.25rem 0.75rem;\n  border-radius: 8px;\n  transition: 0.5s;\n  cursor: pointer;\n}\n.footer[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%]:hover {\n  color: #fff;\n  background-color: rgba(255, 255, 255, 0.2);\n}\n.footer[_ngcontent-%COMP%]    > div.active[_ngcontent-%COMP%] {\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFDRjtBQUFFO0VBQ0UsZ0JBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBRUo7QUFBSTtFQUNFLFdBQUE7RUFDQSwwQ0FBQTtBQUVOO0FBQUk7RUFDRSxXQUFBO0FBRU4iLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9vdGVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgPiBkaXYge1xyXG4gICAgbWFyZ2luOiAwIC41cmVtO1xyXG4gICAgcGFkZGluZzogLjI1cmVtIC43NXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIHRyYW5zaXRpb246IC41cztcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIFxyXG4gICAgJjpob3ZlciB7XHJcbiAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKCNmZmYsIC4yKTtcclxuICAgIH1cclxuICAgICYuYWN0aXZlIHtcclxuICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcbiAgfVxyXG59Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FooterComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-footer',
            templateUrl: './footer.component.html',
            styleUrls: ['./footer.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "ME/D":
    /*!*************************************************************!*\
      !*** ./src/app/modules/notification/notification.module.ts ***!
      \*************************************************************/

    /*! exports provided: NotificationModule */

    /***/
    function MED(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationModule", function () {
        return NotificationModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./notification-factory/notification-factory.component */
      "vi5P");
      /* harmony import */


      var _notification_notification_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./notification/notification.component */
      "/fqK");

      var NotificationModule = function NotificationModule() {
        _classCallCheck(this, NotificationModule);
      };

      NotificationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: NotificationModule
      });
      NotificationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function NotificationModule_Factory(t) {
          return new (t || NotificationModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](NotificationModule, {
          declarations: [_notification_notification_component__WEBPACK_IMPORTED_MODULE_3__["NotificationComponent"], _notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_2__["NotificationFactoryComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"]],
          exports: [_notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_2__["NotificationFactoryComponent"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](NotificationModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [_notification_notification_component__WEBPACK_IMPORTED_MODULE_3__["NotificationComponent"], _notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_2__["NotificationFactoryComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"]],
            exports: [_notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_2__["NotificationFactoryComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "N+5l":
    /*!***********************************************************!*\
      !*** ./src/app/components/overview/overview.component.ts ***!
      \***********************************************************/

    /*! exports provided: OverviewComponent */

    /***/
    function N5l(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OverviewComponent", function () {
        return OverviewComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var OverviewComponent = /*#__PURE__*/function () {
        function OverviewComponent() {
          _classCallCheck(this, OverviewComponent);
        }

        _createClass(OverviewComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return OverviewComponent;
      }();

      OverviewComponent.ɵfac = function OverviewComponent_Factory(t) {
        return new (t || OverviewComponent)();
      };

      OverviewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: OverviewComponent,
        selectors: [["app-overview"]],
        decls: 2,
        vars: 0,
        consts: [["routerLink", "lesson"]],
        template: function OverviewComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " start lesson\n");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvb3ZlcnZpZXcvb3ZlcnZpZXcuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](OverviewComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-overview',
            templateUrl: './overview.component.html',
            styleUrls: ['./overview.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "QAQN":
    /*!*********************************!*\
      !*** ./src/app/models/score.ts ***!
      \*********************************/

    /*! exports provided: Score */

    /***/
    function QAQN(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Score", function () {
        return Score;
      });

      var Score = function Score(score) {
        _classCallCheck(this, Score);

        var _a, _b, _c, _d, _e;

        this.sign = (_a = score.sign) !== null && _a !== void 0 ? _a : '';
        this.level = (_b = score.level) !== null && _b !== void 0 ? _b : 0;
        this.exp = (_c = score.exp) !== null && _c !== void 0 ? _c : 0;
        this.phonType = (_d = score.phonType) !== null && _d !== void 0 ? _d : 'vocale';
        this.avgWpm = (_e = score.avgWpm) !== null && _e !== void 0 ? _e : 0;
        this.unlocked = false;
      }; // - 1 exp fürs abschließen eines wortes
      // - 5 exp fürs fehlerfreie abschließen eines wortes
      // - 1 x wpm für die durchschnittsgeschwindigkeit des Wortes (nur bei 0 Fehlern)
      // Ergebnis wird durch freuquency geteilt => höhere freuquency muss mehrmals geübt werden

      /***/

    },

    /***/
    "Qejw":
    /*!***********************************************!*\
      !*** ./src/app/animations/main-animations.ts ***!
      \***********************************************/

    /*! exports provided: fillHeight, fillWidth, toggleHeight, floatTop */

    /***/
    function Qejw(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "fillHeight", function () {
        return fillHeight;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "fillWidth", function () {
        return fillWidth;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "toggleHeight", function () {
        return toggleHeight;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "floatTop", function () {
        return floatTop;
      });
      /* harmony import */


      var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/animations */
      "GS7A");

      var fillHeight = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('fillHeight', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('start', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        height: 0
      })), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('end', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        height: '100%'
      })), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('start => end', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('{{duration}}')], {
        params: {
          duration: '.5s'
        }
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [// style({ height: '100%' }),
      Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0s')])]);
      var fillWidth = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('fillWidth', [// state('start', style({
      //   width: 0,
      // })),
      // state('end', style({
      //   width: '100%',
      // })),
      Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('start => end', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        width: '{{startWidth}}%'
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('{{duration}}', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        width: '100%'
      }))], {
        params: {
          duration: '.5s',
          startWidth: '0%'
        }
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0s')])]);
      var toggleHeight = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('toggleHeight', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(0.5) translateY(-80px)',
        opacity: 0
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('1s cubic-bezier(.8, -0.6, 0.2, 1.5)', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(1)',
        opacity: 1
      })) // final
      ]), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':leave', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(1)',
        opacity: 1,
        height: '*'
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('1s cubic-bezier(.8, -0.6, 0.2, 1.5)', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(0.5)',
        opacity: 0
      }))])]);
      var floatTop = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('floatTop', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])(':enter', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(1) translateY({{height}})',
        opacity: 0
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('{{speed}}ms cubic-bezier(.3, 0, .5, .5)', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(1) translateY({{centerHeight}})',
        opacity: 1
      })), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('{{speed}}ms cubic-bezier(.5, .5, .4, 1)', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        transform: 'scale(1)',
        opacity: 0
      }))], {
        params: {
          height: '70px',
          centerHeight: '35px',
          speed: 600
        }
      })]);
      /***/
    },

    /***/
    "SWw7":
    /*!********************************************!*\
      !*** ./src/app/services/unlock.service.ts ***!
      \********************************************/

    /*! exports provided: UnlockService */

    /***/
    function SWw7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UnlockService", function () {
        return UnlockService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var UnlockService = /*#__PURE__*/function () {
        function UnlockService() {
          _classCallCheck(this, UnlockService);
        }

        _createClass(UnlockService, null, [{
          key: "unlock",
          value: function unlock(score) {
            var level = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            var amountToUnlock = 5 + Math.floor(level * .25);
            var amountToUnlockV = Math.floor(amountToUnlock / 2);
            var amountToUnlockC = amountToUnlock - amountToUnlockV;
            var unlockedConsonants = 0;
            var unlockedVocales = 0;
            console.log('unlock');

            var _iterator5 = _createForOfIteratorHelper(score),
                _step5;

            try {
              for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
                var s = _step5.value;

                if (s.phonType === 'vocale' && unlockedVocales < amountToUnlockV) {
                  console.log('unlock');
                  s.unlocked = true;
                  unlockedVocales++;
                } else if (s.phonType === 'consonant' && unlockedConsonants < amountToUnlockC) {
                  s.unlocked = true;
                  unlockedConsonants++;
                }

                if (unlockedVocales === amountToUnlockV && unlockedConsonants === amountToUnlockC) {
                  break;
                }
              }
            } catch (err) {
              _iterator5.e(err);
            } finally {
              _iterator5.f();
            }

            return score;
          }
        }]);

        return UnlockService;
      }();

      UnlockService.ɵfac = function UnlockService_Factory(t) {
        return new (t || UnlockService)();
      };

      UnlockService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: UnlockService,
        factory: UnlockService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UnlockService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _components_header_header_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./components/header/header.component */
      "2MiI");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./components/footer/footer.component */
      "LmEr");
      /* harmony import */


      var _modules_notification_notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./modules/notification/notification-factory/notification-factory.component */
      "vi5P");

      var AppComponent = function AppComponent() {
        _classCallCheck(this, AppComponent);
      };

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)();
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        decls: 7,
        vars: 0,
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-header");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "main");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-footer");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "notification-factory");
          }
        },
        directives: [_components_header_header_component__WEBPACK_IMPORTED_MODULE_1__["HeaderComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"], _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"], _modules_notification_notification_factory_notification_factory_component__WEBPACK_IMPORTED_MODULE_4__["NotificationFactoryComponent"]],
        styles: ["main[_ngcontent-%COMP%] {\n  min-height: 90vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQUE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1haW4ge1xyXG4gIG1pbi1oZWlnaHQ6IDkwdmg7XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app-routing.module */
      "vY5A");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./components/overview/overview.component */
      "N+5l");
      /* harmony import */


      var _components_lesson_lesson_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./components/lesson/lesson.component */
      "JiyW");
      /* harmony import */


      var _components_phonset_phonset_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./components/phonset/phonset.component */
      "i8Fj");
      /* harmony import */


      var _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./components/header/header.component */
      "2MiI");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/forms */
      "s7LF");
      /* harmony import */


      var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./components/footer/footer.component */
      "LmEr");
      /* harmony import */


      var _components_progress_progress_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./components/progress/progress.component */
      "1VCC");
      /* harmony import */


      var _modules_notification_notification_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./modules/notification/notification.module */
      "ME/D");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "omvX");
      /* harmony import */


      var _components_lesson_working_on_working_on_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./components/lesson/working-on/working-on.component */
      "f5OE");

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function AppModule_Factory(t) {
          return new (t || AppModule)();
        },
        providers: [],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"], _modules_notification_notification_module__WEBPACK_IMPORTED_MODULE_11__["NotificationModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_4__["OverviewComponent"], _components_lesson_lesson_component__WEBPACK_IMPORTED_MODULE_5__["LessonComponent"], _components_phonset_phonset_component__WEBPACK_IMPORTED_MODULE_6__["PhonsetComponent"], _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"], _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"], _components_progress_progress_component__WEBPACK_IMPORTED_MODULE_10__["ProgressComponent"], _components_lesson_working_on_working_on_component__WEBPACK_IMPORTED_MODULE_13__["WorkingOnComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"], _modules_notification_notification_module__WEBPACK_IMPORTED_MODULE_11__["NotificationModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_4__["OverviewComponent"], _components_lesson_lesson_component__WEBPACK_IMPORTED_MODULE_5__["LessonComponent"], _components_phonset_phonset_component__WEBPACK_IMPORTED_MODULE_6__["PhonsetComponent"], _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"], _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"], _components_progress_progress_component__WEBPACK_IMPORTED_MODULE_10__["ProgressComponent"], _components_lesson_working_on_working_on_component__WEBPACK_IMPORTED_MODULE_13__["WorkingOnComponent"]],
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"], _modules_notification_notification_module__WEBPACK_IMPORTED_MODULE_11__["NotificationModule"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "f5OE":
    /*!**********************************************************************!*\
      !*** ./src/app/components/lesson/working-on/working-on.component.ts ***!
      \**********************************************************************/

    /*! exports provided: WorkingOnComponent */

    /***/
    function f5OE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WorkingOnComponent", function () {
        return WorkingOnComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_data_level_strategy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/data/level.strategy */
      "ygmV");
      /* harmony import */


      var src_app_models_score__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/models/score */
      "QAQN");
      /* harmony import */


      var _progress_progress_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../progress/progress.component */
      "1VCC");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = function _c0(a0, a1, a2) {
        return {
          max: a0,
          value: a1,
          label: a2
        };
      };

      var WorkingOnComponent = /*#__PURE__*/function () {
        function WorkingOnComponent() {
          _classCallCheck(this, WorkingOnComponent);
        }

        _createClass(WorkingOnComponent, [{
          key: "maxExp",
          get: function get() {
            return src_app_data_level_strategy__WEBPACK_IMPORTED_MODULE_1__["LevelStrategy"].maxExp(this.score.level);
          }
        }]);

        return WorkingOnComponent;
      }();

      WorkingOnComponent.ɵfac = function WorkingOnComponent_Factory(t) {
        return new (t || WorkingOnComponent)();
      };

      WorkingOnComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: WorkingOnComponent,
        selectors: [["app-working-on"]],
        inputs: {
          score: "score"
        },
        decls: 7,
        vars: 10,
        consts: [[1, "working-on"], [1, "label"], [3, "progress"], [1, "speed"]],
        template: function WorkingOnComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-progress", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](6, "number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.score.sign, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("progress", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](6, _c0, ctx.maxExp, ctx.score.exp, "Level " + ctx.score.level));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" \xBB ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](6, 3, ctx.score.avgWpm, "1.0-0"), " \xAB ");
          }
        },
        directives: [_progress_progress_component__WEBPACK_IMPORTED_MODULE_3__["ProgressComponent"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["DecimalPipe"]],
        styles: [".working-on[_ngcontent-%COMP%] {\n  min-width: 80px;\n  width: 10%;\n  min-height: 80px;\n}\n.working-on[_ngcontent-%COMP%]   .label[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.working-on[_ngcontent-%COMP%]   .speed[_ngcontent-%COMP%] {\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sZXNzb24vd29ya2luZy1vbi93b3JraW5nLW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQUNGO0FBQUU7RUFDRSxrQkFBQTtBQUVKO0FBQUU7RUFDRSxrQkFBQTtBQUVKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9sZXNzb24vd29ya2luZy1vbi93b3JraW5nLW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndvcmtpbmctb24ge1xyXG4gIG1pbi13aWR0aDogODBweDtcclxuICB3aWR0aDogMTAlO1xyXG4gIG1pbi1oZWlnaHQ6IDgwcHg7XHJcbiAgLmxhYmVsIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLnNwZWVkIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WorkingOnComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-working-on',
            templateUrl: './working-on.component.html',
            styleUrls: ['./working-on.component.scss']
          }]
        }], null, {
          score: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "hDew":
    /*!*************************************************!*\
      !*** ./src/app/services/text-reader.service.ts ***!
      \*************************************************/

    /*! exports provided: PhonAmount, TextReaderService */

    /***/
    function hDew(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PhonAmount", function () {
        return PhonAmount;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TextReaderService", function () {
        return TextReaderService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _phon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./phon.service */
      "vgeL");

      var PhonAmount = function PhonAmount(sign) {
        var amount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

        _classCallCheck(this, PhonAmount);

        this.sign = sign;
        this.amount = amount;
      };

      var TextReaderService = /*#__PURE__*/function () {
        function TextReaderService(_phonService) {
          _classCallCheck(this, TextReaderService);

          this._phonService = _phonService;
        }

        _createClass(TextReaderService, [{
          key: "getPhons",
          value: function getPhons(text) {
            text = text.toLocaleLowerCase();

            if (text.includes(' ')) {
              if (text.match(new RegExp(/\r?\n|\r/))) {
                text = text.replace(new RegExp(/\r?\n|\r/, 'gm'), ' ');
              }

              var words = text.split(' ');
              return this.splitWordRecursiv(words);
            } else {
              return this.splitWordRecursiv([text]);
            }
          }
        }, {
          key: "getPhonAmount",
          value: function getPhonAmount(text) {
            var ignorecharsInput = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

            var _iterator6 = _createForOfIteratorHelper(ignorecharsInput.split('')),
                _step6;

            try {
              for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
                var i = _step6.value;
                // if (i === '\\') { reg = '\\\\'; }
                text = text.replace(new RegExp('\\' + i, 'g'), ' ');
              }
            } catch (err) {
              _iterator6.e(err);
            } finally {
              _iterator6.f();
            }

            var phonAmount = [];
            var phons = this.getPhons(text);

            var _iterator7 = _createForOfIteratorHelper(phons),
                _step7;

            try {
              var _loop = function _loop() {
                var phon = _step7.value;

                if (phonAmount.find(function (p) {
                  return p.sign === phon;
                })) {
                  phonAmount.find(function (p) {
                    return p.sign === phon;
                  }).amount++;
                } else {
                  phonAmount.push(new PhonAmount(phon));
                }
              };

              for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
                _loop();
              }
            } catch (err) {
              _iterator7.e(err);
            } finally {
              _iterator7.f();
            }

            phonAmount = phonAmount.filter(function (p) {
              return p.sign !== '' && p.sign !== ' ' && p.sign !== '\\n';
            });
            return phonAmount.sort(function (a, b) {
              if (a.sign.length > b.sign.length) {
                return 1;
              } else if (a.sign.length === b.sign.length) {
                return a.amount < b.amount ? 1 : -1;
              } else {
                return -1;
              }
            });
          }
        }, {
          key: "splitWordRecursiv",
          value: function splitWordRecursiv(text) {
            var vocals = this._phonService.vocals;
            var newSplits = [];
            var wordsToRemove = [];

            var _iterator8 = _createForOfIteratorHelper(text),
                _step8;

            try {
              for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
                var t = _step8.value;

                var _iterator9 = _createForOfIteratorHelper(vocals),
                    _step9;

                try {
                  for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
                    var v = _step9.value;

                    if (t.includes(v.sign) && t.length > v.sign.length) {
                      var amount = t.match(new RegExp(v.sign, 'g')).length;

                      for (var c = 0; c < amount; c++) {
                        newSplits.push(v.sign);
                      }

                      var splitts = t.split(v.sign).filter(function (te) {
                        return te;
                      });
                      wordsToRemove.push(t);
                      newSplits.push.apply(newSplits, _toConsumableArray(this.splitWordRecursiv(splitts)));
                      break;
                    }
                  }
                } catch (err) {
                  _iterator9.e(err);
                } finally {
                  _iterator9.f();
                }
              }
            } catch (err) {
              _iterator8.e(err);
            } finally {
              _iterator8.f();
            }

            var _loop2 = function _loop2() {
              var word = _wordsToRemove[_i];
              var i = text.findIndex(function (te) {
                return te === word;
              });

              if (i !== -1) {
                text.splice(i, 1);
              }
            };

            for (var _i = 0, _wordsToRemove = wordsToRemove; _i < _wordsToRemove.length; _i++) {
              _loop2();
            }

            return [].concat.apply([].concat(newSplits, _toConsumableArray(text.filter(function (s) {
              return s !== '';
            }))));
          }
        }]);

        return TextReaderService;
      }();

      TextReaderService.ɵfac = function TextReaderService_Factory(t) {
        return new (t || TextReaderService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_phon_service__WEBPACK_IMPORTED_MODULE_1__["PhonService"]));
      };

      TextReaderService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: TextReaderService,
        factory: TextReaderService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TextReaderService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _phon_service__WEBPACK_IMPORTED_MODULE_1__["PhonService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "i8Fj":
    /*!*********************************************************!*\
      !*** ./src/app/components/phonset/phonset.component.ts ***!
      \*********************************************************/

    /*! exports provided: PhonsetComponent */

    /***/
    function i8Fj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PhonsetComponent", function () {
        return PhonsetComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var src_app_services_phon_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/app/services/phon.service */
      "vgeL");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var src_app_services_text_reader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/app/services/text-reader.service */
      "hDew");
      /* harmony import */


      var src_app_modules_notification_notification_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/app/modules/notification/notification.service */
      "IBgE");
      /* harmony import */


      var src_app_models_phon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/models/phon */
      "GPe3");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/forms */
      "s7LF");

      function PhonsetComponent_option_4_Template(rf, ctx) {
        if (rf & 1) {
          var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhonsetComponent_option_4_Template_option_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

            var set_r3 = ctx.$implicit;

            var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r4.useSet(set_r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var set_r3 = ctx.$implicit;

          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("selected", ctx_r0.nameInput === set_r3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", set_r3, " ");
        }
      }

      function PhonsetComponent_div_25_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var phon_r7 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" (", phon_r7.amount, "x) ", phon_r7.sign, " ");
        }
      }

      function PhonsetComponent_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, PhonsetComponent_div_25_div_1_Template, 2, 2, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.generateResult);
        }
      }

      function PhonsetComponent_div_26_Template(rf, ctx) {
        if (rf & 1) {
          var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " warning! Importing new parameters to this set will reset your Hero! It would be better to do this on a empty ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhonsetComponent_div_26_Template_button_click_3_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r8.initSet();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Generate Set ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var PhonsetComponent = /*#__PURE__*/function () {
        function PhonsetComponent(_phonService, _tr, _n) {
          _classCallCheck(this, PhonsetComponent);

          this._phonService = _phonService;
          this._tr = _tr;
          this._n = _n;
          this.nameInput = '';
          this.vocaleInput = '';
          this.sampleTextInput = '';
          this.ignorecharsInput = '\'"`,./\\?!:;^_-$#=*+&|%«»–123456789(){}[]<>';
          this._alive = true;
        }

        _createClass(PhonsetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this4 = this;

            this._phonService.curSet.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeWhile"])(function () {
              return _this4._alive;
            })).subscribe(function (phonSet) {
              _this4.nameInput = phonSet.name;
              _this4.sampleTextInput = '';
              _this4.generateResult = null;
              _this4.vocaleInput = phonSet.vocales.map(function (v) {
                return v.sign;
              }).join(',');
              console.log('this.vocaleInput', _this4.vocaleInput);
              _this4.sets = _this4._phonService.sets;
            });
          }
        }, {
          key: "useSet",
          value: function useSet() {
            console.log('todo');
          }
        }, {
          key: "createSet",
          value: function createSet() {
            this._phonService.createSet();
          }
        }, {
          key: "changeName",
          value: function changeName(evt) {
            this._phonService.changeName(evt.target.value);
          }
        }, {
          key: "changeCurSet",
          value: function changeCurSet(evt) {
            this._phonService.changeCurSet(evt.target.value);
          }
        }, {
          key: "generateSet",
          value: function generateSet() {
            if (this.sampleTextInput.length < 100) {
              this._n.show('please enter at least 100 letters'); // return;

            }

            this._phonService.setVocals(this.vocaleInput.split(','));

            this.generateResult = this._tr.getPhonAmount(this.sampleTextInput, this.ignorecharsInput);
            console.log('this.generateResult', this.generateResult);
          }
        }, {
          key: "initSet",
          value: function initSet() {
            var maxAmount = this.generateResult.map(function (g) {
              return g.amount;
            }).reduce(function (p, v) {
              return p < v ? p : v;
            });
            var vocales = [];
            var consonants = [];

            var _iterator10 = _createForOfIteratorHelper(this.generateResult),
                _step10;

            try {
              for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
                var phon = _step10.value;

                if (this.vocaleInput.includes(phon.sign)) {
                  vocales.push(new src_app_models_phon__WEBPACK_IMPORTED_MODULE_5__["Phon"](phon.sign, 1 - maxAmount / phon.amount));
                } else {
                  consonants.push(new src_app_models_phon__WEBPACK_IMPORTED_MODULE_5__["Phon"](phon.sign, 1 - maxAmount / phon.amount));
                }
              }
            } catch (err) {
              _iterator10.e(err);
            } finally {
              _iterator10.f();
            }

            this._phonService.initSet(vocales, consonants);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this._alive = false;
          }
        }]);

        return PhonsetComponent;
      }();

      PhonsetComponent.ɵfac = function PhonsetComponent_Factory(t) {
        return new (t || PhonsetComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_phon_service__WEBPACK_IMPORTED_MODULE_1__["PhonService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_text_reader_service__WEBPACK_IMPORTED_MODULE_3__["TextReaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_modules_notification_notification_service__WEBPACK_IMPORTED_MODULE_4__["NotificationService"]));
      };

      PhonsetComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: PhonsetComponent,
        selectors: [["app-phonset"]],
        decls: 27,
        vars: 7,
        consts: [[3, "change"], [3, "selected", "click", 4, "ngFor", "ngForOf"], [1, "btn", 3, "click"], ["type", "text", 3, "ngModel", "input", "ngModelChange"], [3, "ngModel", "ngModelChange"], ["class", "generated-results", 4, "ngIf"], [4, "ngIf"], [3, "selected", "click"], [1, "generated-results"], ["class", "result", 4, "ngFor", "ngForOf"], [1, "result"]],
        template: function PhonsetComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Sets: ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "select", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function PhonsetComponent_Template_select_change_3_listener($event) {
              return ctx.changeCurSet($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, PhonsetComponent_option_4_Template, 2, 2, "option", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhonsetComponent_Template_button_click_5_listener() {
              return ctx.createSet();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " new Set ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " your Hero name ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function PhonsetComponent_Template_input_input_8_listener($event) {
              return ctx.changeName($event);
            })("ngModelChange", function PhonsetComponent_Template_input_ngModelChange_8_listener($event) {
              return ctx.nameInput = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Vocales: ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "enter multiple vocales seperated by a ,");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "textarea", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PhonsetComponent_Template_textarea_ngModelChange_13_listener($event) {
              return ctx.vocaleInput = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " ignorechars: ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "enter multiple characters you want to ignore in the sample text");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "textarea", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PhonsetComponent_Template_textarea_ngModelChange_18_listener($event) {
              return ctx.ignorecharsInput = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " Sample Text: ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "textarea", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PhonsetComponent_Template_textarea_ngModelChange_21_listener($event) {
              return ctx.sampleTextInput = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PhonsetComponent_Template_button_click_22_listener() {
              return ctx.generateSet();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Generate Set ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, " result ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, PhonsetComponent_div_25_Template, 2, 1, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, PhonsetComponent_div_26_Template, 5, 0, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.sets);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.nameInput);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.vocaleInput);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.ignorecharsInput);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.sampleTextInput);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.generateResult);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.generateResult);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵangular_packages_forms_forms_x"]],
        styles: ["[_nghost-%COMP%] {\n  margin: 2rem;\n  position: relative;\n  display: block;\n}\n\n.generated-results[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-wrap: wrap;\n  max-width: 80%;\n  margin: 0 auto;\n}\n\n.generated-results[_ngcontent-%COMP%]   .result[_ngcontent-%COMP%] {\n  margin: 1rem;\n  min-width: 8rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9waG9uc2V0L3Bob25zZXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQUNGOztBQUFFO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUFFSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGhvbnNldC9waG9uc2V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gIG1hcmdpbjogMnJlbTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5nZW5lcmF0ZWQtcmVzdWx0cyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxuICBtYXgtd2lkdGg6IDgwJTtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICAucmVzdWx0IHtcclxuICAgIG1hcmdpbjogMXJlbTtcclxuICAgIG1pbi13aWR0aDogOHJlbTtcclxuICB9XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PhonsetComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-phonset',
            templateUrl: './phonset.component.html',
            styleUrls: ['./phonset.component.scss']
          }]
        }], function () {
          return [{
            type: src_app_services_phon_service__WEBPACK_IMPORTED_MODULE_1__["PhonService"]
          }, {
            type: src_app_services_text_reader_service__WEBPACK_IMPORTED_MODULE_3__["TextReaderService"]
          }, {
            type: src_app_modules_notification_notification_service__WEBPACK_IMPORTED_MODULE_4__["NotificationService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "kKNg":
    /*!***************************************************!*\
      !*** ./src/app/services/word-generate.service.ts ***!
      \***************************************************/

    /*! exports provided: WordGenerateService */

    /***/
    function kKNg(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WordGenerateService", function () {
        return WordGenerateService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _data_training_mode_strategy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../data/training-mode.strategy */
      "o/5b");
      /* harmony import */


      var _phon_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./phon.service */
      "vgeL");
      /* harmony import */


      var _setting_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./setting.service */
      "KQZp");

      var WordGenerateService = /*#__PURE__*/function () {
        function WordGenerateService(_phonService, _settingService) {
          _classCallCheck(this, WordGenerateService);

          this._phonService = _phonService;
          this._settingService = _settingService;
        }

        _createClass(WordGenerateService, [{
          key: "get",
          value: function get() {
            var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 5;
            console.log('lengtx', length);
            var wordStructure = [];
            var word = '';
            var lastPhonType = 'vocale';

            if (Math.random() > .5) {
              // start with vocale or consonant
              lastPhonType = 'vocale';
            } else {
              lastPhonType = 'consonant';
            }

            word = this.findOneRandom(this._phonService.getBadScore(lastPhonType));
            wordStructure.push(lastPhonType); // console.log('word', word);

            var i = 0;

            while (word.length < length && i < 999) {
              var freeLength = length - word.length;

              if (Math.random() > .5) {
                // add next left or right
                // right = at the end
                var typeAtTheEnd = wordStructure[wordStructure.length - 1];
                lastPhonType = typeAtTheEnd === 'consonant' ? 'vocale' : 'consonant';
                word += this.getRndWordPart(lastPhonType, freeLength);
                wordStructure.push(lastPhonType);
              } else {
                // left = at the begin
                var typeAtTheBegin = wordStructure[0];
                lastPhonType = typeAtTheBegin === 'consonant' ? 'vocale' : 'consonant';
                word = this.getRndWordPart(lastPhonType, freeLength) + word;
                wordStructure.unshift(lastPhonType);
              }

              i++;
            }

            return word;
          }
        }, {
          key: "getRndWordPart",
          value: function getRndWordPart(type, maxlength) {
            if (Math.random() > _data_training_mode_strategy__WEBPACK_IMPORTED_MODULE_1__["TrainingModeStrategy"].chanceForAlernatives(this._settingService.get.trainingMode)) {
              // find another bad score word
              return this.findOneRandom(this._phonService.getBadScore(type).filter(function (p) {
                return p.sign.length <= maxlength;
              }));
            } else {
              return this.findOneRandom(this._phonService.getUnlockedScore(type).filter(function (p) {
                return p.sign.length <= maxlength;
              }));
            }
          }
        }, {
          key: "findOneRandom",
          value: function findOneRandom(phon) {
            if (phon.length === 0) return '';
            return phon[Math.floor(Math.random() * phon.length)].sign;
          }
        }]);

        return WordGenerateService;
      }();

      WordGenerateService.ɵfac = function WordGenerateService_Factory(t) {
        return new (t || WordGenerateService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_phon_service__WEBPACK_IMPORTED_MODULE_2__["PhonService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"]));
      };

      WordGenerateService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: WordGenerateService,
        factory: WordGenerateService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](WordGenerateService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _phon_service__WEBPACK_IMPORTED_MODULE_2__["PhonService"]
          }, {
            type: _setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "n90K":
    /*!*********************************************!*\
      !*** ./src/app/services/storage.service.ts ***!
      \*********************************************/

    /*! exports provided: StorageService */

    /***/
    function n90K(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "StorageService", function () {
        return StorageService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var StorageService = /*#__PURE__*/function () {
        function StorageService() {
          _classCallCheck(this, StorageService);

          this.storage = localStorage;
          this.cache = {};
        }

        _createClass(StorageService, [{
          key: "get",
          value: function get(key, defaultValue) {
            var noCache = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            // find in cache
            if (!noCache && !!this.cache[key]) return this.cache[key];
            var value = JSON.parse(this.storage.getItem(key));
            if (value === null && defaultValue) return defaultValue;
            return value;
          }
        }, {
          key: "save",
          value: function save(key, value) {
            var noCache = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            if (!noCache) this.cache[key] = JSON.stringify(value);
            this.storage.setItem(key, JSON.stringify(value));
          }
        }]);

        return StorageService;
      }();

      StorageService.ɵfac = function StorageService_Factory(t) {
        return new (t || StorageService)();
      };

      StorageService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: StorageService,
        factory: StorageService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](StorageService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "o/5b":
    /*!************************************************!*\
      !*** ./src/app/data/training-mode.strategy.ts ***!
      \************************************************/

    /*! exports provided: TrainingModeStrategy */

    /***/
    function o5b(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TrainingModeStrategy", function () {
        return TrainingModeStrategy;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_setting_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../services/setting.service */
      "KQZp");

      var TrainingModeStrategy = /*#__PURE__*/function () {
        function TrainingModeStrategy() {
          _classCallCheck(this, TrainingModeStrategy);
        }

        _createClass(TrainingModeStrategy, null, [{
          key: "amount",
          value: function amount(trainingMode) {
            switch (trainingMode) {
              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].random:
                return 15;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].mixed:
                return 10;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].training:
                return 5;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].focus:
                return 2;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].hardFocus:
                return 1;
            }
          }
        }, {
          key: "chanceForAlernatives",
          value: function chanceForAlernatives(trainingMode) {
            switch (trainingMode) {
              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].random:
                return 1;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].mixed:
                return .1;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].training:
                return .5;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].focus:
                return .3;

              case _services_setting_service__WEBPACK_IMPORTED_MODULE_1__["ETrainingMode"].hardFocus:
                return .3;
            }
          }
        }]);

        return TrainingModeStrategy;
      }();

      TrainingModeStrategy.ɵfac = function TrainingModeStrategy_Factory(t) {
        return new (t || TrainingModeStrategy)();
      };

      TrainingModeStrategy.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: TrainingModeStrategy,
        factory: TrainingModeStrategy.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TrainingModeStrategy, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "rk0U":
    /*!********************************************!*\
      !*** ./src/app/services/reward.service.ts ***!
      \********************************************/

    /*! exports provided: RewardService */

    /***/
    function rk0U(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RewardService", function () {
        return RewardService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _data_level_strategy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../data/level.strategy */
      "ygmV");
      /* harmony import */


      var _phon_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./phon.service */
      "vgeL");
      /* harmony import */


      var _text_reader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./text-reader.service */
      "hDew");

      var RewardService = /*#__PURE__*/function () {
        function RewardService(_tr, _phonService) {
          _classCallCheck(this, RewardService);

          this._tr = _tr;
          this._phonService = _phonService;
        }

        _createClass(RewardService, [{
          key: "getReward",
          value: function getReward(word, duration, missspelled) {
            var _this5 = this;

            var _a;

            var wpm = 60 * (word.length / 5) / (duration / 1000);

            var wordParts = this._tr.getPhonAmount(word);

            var exp = missspelled === 0 ? 1 : 0;
            exp += Math.floor(wpm / 10);

            this._phonService.addExp(exp);

            var _iterator11 = _createForOfIteratorHelper(wordParts),
                _step11;

            try {
              var _loop3 = function _loop3() {
                var part = _step11.value;

                var score = _this5._phonService.score.find(function (s) {
                  return s.sign === part.sign;
                });

                if (score) {
                  var phon = _this5._phonService.findPhon(part.sign, score.phonType);

                  var newExp = exp * (1 / 2 * ((_a = phon === null || phon === void 0 ? void 0 : phon.frequency) !== null && _a !== void 0 ? _a : 1));
                  console.log('newExp', newExp);
                  score.avgWpm = (score.avgWpm * 1.99 + wpm * 0.01) / 2;
                  score.exp += newExp * part.amount;

                  if (score.exp > _data_level_strategy__WEBPACK_IMPORTED_MODULE_1__["LevelStrategy"].maxExp(score.level)) {
                    score.level++;
                    score.exp -= _data_level_strategy__WEBPACK_IMPORTED_MODULE_1__["LevelStrategy"].maxExp(score.level);
                  }
                }
              };

              for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
                _loop3();
              }
            } catch (err) {
              _iterator11.e(err);
            } finally {
              _iterator11.f();
            }
          }
        }]);

        return RewardService;
      }();

      RewardService.ɵfac = function RewardService_Factory(t) {
        return new (t || RewardService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_text_reader_service__WEBPACK_IMPORTED_MODULE_3__["TextReaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_phon_service__WEBPACK_IMPORTED_MODULE_2__["PhonService"]));
      };

      RewardService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: RewardService,
        factory: RewardService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RewardService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _text_reader_service__WEBPACK_IMPORTED_MODULE_3__["TextReaderService"]
          }, {
            type: _phon_service__WEBPACK_IMPORTED_MODULE_2__["PhonService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "vY5A":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function vY5A(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _components_lesson_lesson_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./components/lesson/lesson.component */
      "JiyW");
      /* harmony import */


      var _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./components/overview/overview.component */
      "N+5l");
      /* harmony import */


      var _components_phonset_phonset_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./components/phonset/phonset.component */
      "i8Fj");

      var routes = [{
        path: '',
        component: _components_overview_overview_component__WEBPACK_IMPORTED_MODULE_3__["OverviewComponent"]
      }, {
        path: 'lesson',
        component: _components_lesson_lesson_component__WEBPACK_IMPORTED_MODULE_2__["LessonComponent"]
      }, {
        path: 'set',
        component: _components_phonset_phonset_component__WEBPACK_IMPORTED_MODULE_4__["PhonsetComponent"]
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppRoutingModule
      });
      AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppRoutingModule_Factory(t) {
          return new (t || AppRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "vgeL":
    /*!******************************************!*\
      !*** ./src/app/services/phon.service.ts ***!
      \******************************************/

    /*! exports provided: PhonService */

    /***/
    function vgeL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PhonService", function () {
        return PhonService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _data_level_strategy__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../data/level.strategy */
      "ygmV");
      /* harmony import */


      var _data_phons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../data/phons */
      "CfCB");
      /* harmony import */


      var _data_training_mode_strategy__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../data/training-mode.strategy */
      "o/5b");
      /* harmony import */


      var _models_phon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../models/phon */
      "GPe3");
      /* harmony import */


      var _models_phonset__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../models/phonset */
      "1XqU");
      /* harmony import */


      var _setting_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./setting.service */
      "KQZp");
      /* harmony import */


      var _storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./storage.service */
      "n90K");
      /* harmony import */


      var _unlock_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./unlock.service */
      "SWw7");

      var PhonService = /*#__PURE__*/function () {
        function PhonService(_storageService, _settingService) {
          _classCallCheck(this, PhonService);

          this._storageService = _storageService;
          this._settingService = _settingService;
          this._curSet = 0;
          this._ignoredSigns = ['/'];
          this.curSet = new rxjs__WEBPACK_IMPORTED_MODULE_1__["ReplaySubject"]();
          this.almostLvlUp = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
          this._phonsets = this._storageService.get('phonsets');
          this._curSet = this._storageService.get('curset');

          if (!this._phonsets) {
            this.initPhonsets();
          }

          this.curSet.next(this._phonsets[this._curSet]);
        }

        _createClass(PhonService, [{
          key: "maxVocaleLength",
          get: function get() {
            var max = 0;

            var _iterator12 = _createForOfIteratorHelper(this._phonsets[this._curSet].vocales),
                _step12;

            try {
              for (_iterator12.s(); !(_step12 = _iterator12.n()).done;) {
                var v = _step12.value;

                if (v.sign.length > max) {
                  max = v.sign.length;
                }
              }
            } catch (err) {
              _iterator12.e(err);
            } finally {
              _iterator12.f();
            }

            return max;
          }
        }, {
          key: "maxConsonantLength",
          get: function get() {
            var max = 0;

            var _iterator13 = _createForOfIteratorHelper(this._phonsets[this._curSet].vocales),
                _step13;

            try {
              for (_iterator13.s(); !(_step13 = _iterator13.n()).done;) {
                var c = _step13.value;

                if (c.sign.length > max) {
                  max = c.sign.length;
                }
              }
            } catch (err) {
              _iterator13.e(err);
            } finally {
              _iterator13.f();
            }

            return max;
          }
        }, {
          key: "save",
          value: function save() {
            this._storageService.save('phonsets', this._phonsets);

            this._storageService.save('curset', this._curSet);

            this.curSet.next(this._phonsets[this._curSet]);
          }
        }, {
          key: "sets",
          get: function get() {
            return this._phonsets.map(function (p) {
              return p.name;
            });
          }
        }, {
          key: "vocals",
          get: function get() {
            return this._phonsets[this._curSet].vocales;
          } // public get consonants(): Phon[] {
          //   return this._phonsets[this._curSet].consonants;
          // }

        }, {
          key: "score",
          get: function get() {
            return this._phonsets[this._curSet].score;
          }
        }, {
          key: "name",
          get: function get() {
            return this._phonsets[this._curSet].name;
          }
        }, {
          key: "ignoredSigns",
          get: function get() {
            return this._ignoredSigns;
          }
        }, {
          key: "findPhon",
          value: function findPhon(sign, type) {
            if (type === 'vocale') {
              return this._phonsets[this._curSet].vocales.find(function (v) {
                return v.sign === sign;
              });
            } else {
              return this._phonsets[this._curSet].consonants.find(function (v) {
                return v.sign === sign;
              });
            }
          }
        }, {
          key: "getBadScore",
          value: function getBadScore(type) {
            var amount = _data_training_mode_strategy__WEBPACK_IMPORTED_MODULE_4__["TrainingModeStrategy"].amount(this._settingService.get.trainingMode);

            var phons = this._phonsets[this._curSet].score.filter(function (s) {
              return s.phonType === type && s.unlocked;
            });

            var lowestLevel = phons.reduce(function (p, v) {
              return p < v.level ? p : v.level;
            }, 999);
            var badPhons = phons.filter(function (p) {
              return p.level === lowestLevel;
            }).slice(0, amount);

            if (badPhons.length < amount) {
              badPhons.push.apply(badPhons, _toConsumableArray(phons.filter(function (p) {
                return p.level === lowestLevel + 1;
              }).slice(0, amount - badPhons.length)));
            }

            return badPhons;
          }
        }, {
          key: "getUnlockedScore",
          value: function getUnlockedScore(type) {
            return this._phonsets[this._curSet].score.filter(function (s) {
              return s.phonType === type && s.unlocked;
            });
          }
        }, {
          key: "createSet",
          value: function createSet() {
            this._phonsets.push(new _models_phonset__WEBPACK_IMPORTED_MODULE_6__["PhonSet"]('Hero', _data_phons__WEBPACK_IMPORTED_MODULE_3__["vocales"], _data_phons__WEBPACK_IMPORTED_MODULE_3__["consonants"]));

            this._curSet = this._phonsets.length - 1;
            this.save();
          }
        }, {
          key: "initSet",
          value: function initSet(voc, con) {
            var oldName = this._phonsets[this._curSet].name;
            this._phonsets[this._curSet] = new _models_phonset__WEBPACK_IMPORTED_MODULE_6__["PhonSet"](oldName, voc, con);
            this._phonsets[this._curSet].score = _unlock_service__WEBPACK_IMPORTED_MODULE_9__["UnlockService"].unlock(this._phonsets[this._curSet].score);
            this.save();
          }
        }, {
          key: "changeName",
          value: function changeName(newName) {
            this._phonsets[this._curSet].name = newName;
            this.save();
          }
        }, {
          key: "setVocals",
          value: function setVocals(voc) {
            this._phonsets[this._curSet].vocales = voc.map(function (v) {
              return new _models_phon__WEBPACK_IMPORTED_MODULE_5__["Phon"](v);
            });
          }
        }, {
          key: "changeCurSet",
          value: function changeCurSet(name) {
            var i = this._phonsets.findIndex(function (p) {
              return p.name === name;
            });

            if (i !== -1) {
              this._curSet = i;
              this.save();
            }
          }
        }, {
          key: "initPhonsets",
          value: function initPhonsets() {
            this._phonsets = [new _models_phonset__WEBPACK_IMPORTED_MODULE_6__["PhonSet"]('Hero', _data_phons__WEBPACK_IMPORTED_MODULE_3__["vocales"], _data_phons__WEBPACK_IMPORTED_MODULE_3__["consonants"])];
            this._curSet = 0;
            this._phonsets[0].score = _unlock_service__WEBPACK_IMPORTED_MODULE_9__["UnlockService"].unlock(this._phonsets[0].score);
            this.save();
          }
        }, {
          key: "addExp",
          value: function addExp(exp) {
            console.log('addExp', exp);

            if (!this._phonsets[this._curSet].exp) {
              this._phonsets[this._curSet].exp = 0;
            } // DELETE THIS


            this._phonsets[this._curSet].exp += exp;

            if (this._phonsets[this._curSet].exp > _data_level_strategy__WEBPACK_IMPORTED_MODULE_2__["LevelStrategy"].maxMainExp(this._phonsets[this._curSet].level)) {
              this._phonsets[this._curSet].exp -= _data_level_strategy__WEBPACK_IMPORTED_MODULE_2__["LevelStrategy"].maxMainExp(this._phonsets[this._curSet].level);
              this._phonsets[this._curSet].level++;
            }

            this.curSet.next(this._phonsets[this._curSet]);
            this.checkAlmostlvlUp();
          }
        }, {
          key: "checkAlmostlvlUp",
          value: function checkAlmostlvlUp() {
            var old = this.almostLvlUp.getValue();

            for (var i = old.length; i >= 0; i--) {
              if (old[i] && _data_level_strategy__WEBPACK_IMPORTED_MODULE_2__["LevelStrategy"].perc(old[i].exp, old[i].level) > .1 && _data_level_strategy__WEBPACK_IMPORTED_MODULE_2__["LevelStrategy"].perc(old[i].exp, old[i].level) < .95) {
                old.splice(i, 1);
              }
            }

            var scores = this._phonsets[this._curSet].score.filter(function (s) {
              return _data_level_strategy__WEBPACK_IMPORTED_MODULE_2__["LevelStrategy"].perc(s.exp, s.level) > .95 && !old.map(function (o) {
                return o.sign;
              }).includes(s.sign);
            });

            this.almostLvlUp.next([].concat(_toConsumableArray(old), _toConsumableArray(scores)));
          }
        }]);

        return PhonService;
      }();

      PhonService.ɵfac = function PhonService_Factory(t) {
        return new (t || PhonService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_setting_service__WEBPACK_IMPORTED_MODULE_7__["SettingService"]));
      };

      PhonService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: PhonService,
        factory: PhonService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PhonService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"]
          }, {
            type: _setting_service__WEBPACK_IMPORTED_MODULE_7__["SettingService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "vi5P":
    /*!*********************************************************************************************!*\
      !*** ./src/app/modules/notification/notification-factory/notification-factory.component.ts ***!
      \*********************************************************************************************/

    /*! exports provided: NotificationFactoryComponent */

    /***/
    function vi5P(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationFactoryComponent", function () {
        return NotificationFactoryComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var src_app_animations_main_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/animations/main-animations */
      "Qejw");
      /* harmony import */


      var _notification_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../notification.service */
      "IBgE");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _notification_notification_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../notification/notification.component */
      "/fqK");

      function NotificationFactoryComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "notification", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("closeMessage", function NotificationFactoryComponent_div_1_Template_notification_closeMessage_1_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

            var message_r1 = ctx.$implicit;

            var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r2.closeMessage(message_r1);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var message_r1 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("@toggleHeight", undefined);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("message", message_r1);
        }
      }

      var NotificationFactoryComponent = /*#__PURE__*/function () {
        function NotificationFactoryComponent(notificationService) {
          _classCallCheck(this, NotificationFactoryComponent);

          this.notificationService = notificationService;
          this.messages = [];
          this.isAlive = true;
        }

        _createClass(NotificationFactoryComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this6 = this;

            this.notificationService.notifications.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["takeWhile"])(function () {
              return _this6.isAlive;
            })).subscribe(function (message) {
              _this6.messages.splice(0, 0, message);

              if (message.duration > 0) {
                setTimeout(function () {
                  return _this6.removeNotification(message);
                }, message.duration);
              }
            });
          }
        }, {
          key: "removeNotification",
          value: function removeNotification(message) {
            var index = this.messages.indexOf(message);

            if (index !== -1) {
              this.messages.splice(index, 1);
            }
          }
        }, {
          key: "closeMessage",
          value: function closeMessage(message) {
            this.removeNotification(message);
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.isAlive = false;
          }
        }]);

        return NotificationFactoryComponent;
      }();

      NotificationFactoryComponent.ɵfac = function NotificationFactoryComponent_Factory(t) {
        return new (t || NotificationFactoryComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_notification_service__WEBPACK_IMPORTED_MODULE_3__["NotificationService"]));
      };

      NotificationFactoryComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: NotificationFactoryComponent,
        selectors: [["notification-factory"]],
        decls: 2,
        vars: 1,
        consts: [[1, "notificationWrapper"], [4, "ngFor", "ngForOf"], [1, "notification", 3, "message", "closeMessage"]],
        template: function NotificationFactoryComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NotificationFactoryComponent_div_1_Template, 2, 2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.messages);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _notification_notification_component__WEBPACK_IMPORTED_MODULE_5__["NotificationComponent"]],
        styles: [".notificationWrapper[_ngcontent-%COMP%] {\n  position: fixed;\n  top: 10px;\n  left: 0;\n  z-index: 1000;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  pointer-events: none;\n}\n@media (min-width: 960px) {\n  .notificationWrapper[_ngcontent-%COMP%] {\n    top: 50px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9zY3NzL192YXJpYWJsZXMuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLWZhY3Rvcnkvbm90aWZpY2F0aW9uLWZhY3RvcnkuY29tcG9uZW50LnNjc3MiLCJzcmMvc2Nzcy9fbWl4aW5zLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQUE7QUNFQTtFQUNFLGVBQUE7RUFDRCxTQUFBO0VBQ0MsT0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNELHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBQUQ7QUNFQztFRFpEO0lBWUUsU0FBQTtFQUVBO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24tZmFjdG9yeS9ub3RpZmljYXRpb24tZmFjdG9yeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGJyZWFrcG9pbnRzICovXHJcbiR4czogMDtcclxuJHNtOiA2MDBweDtcclxuJG1kOiA5NjBweDtcclxuJGxnOiAxMjgwcHg7XHJcbiR4bDogMTkyMHB4O1xyXG5cclxuXHJcblxyXG5cclxuJGMxOiAjNTgzNGViO1xyXG4kYzM6ICMwMGY3ZmY7XHJcbiRjMjogIzRmMDBjNDtcclxuJGRhcmtncmV5OiAjMTgxODE4O1xyXG4kcmVkOiAjYjEwMTAxO1xyXG5cclxuIiwiQGltcG9ydCAnbWl4aW5zJztcclxuXHJcbi5ub3RpZmljYXRpb25XcmFwcGVyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcblx0dG9wOiAxMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgei1pbmRleDogMTAwMDtcclxuICB3aWR0aDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0ZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuXHRwb2ludGVyLWV2ZW50czogbm9uZTtcclxuXHRAaW5jbHVkZSBtZCB7XHJcblx0XHR0b3A6IDUwcHg7XHJcblx0fVxyXG59IiwiQGltcG9ydCAndmFyaWFibGVzJztcclxuXHJcblxyXG5AbWl4aW4geHMge1xyXG5cdEBtZWRpYSAobWluLXdpZHRoOiAkeHMpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gc20ge1xyXG5cdEBtZWRpYSAobWluLXdpZHRoOiAkc20pIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gbWQge1xyXG5cdEBtZWRpYSAobWluLXdpZHRoOiAkbWQpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gbGcge1xyXG5cdEBtZWRpYSAobWluLXdpZHRoOiAkbGcpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4geGwge1xyXG5cdEBtZWRpYSAobWluLXdpZHRoOiAkeGwpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gbWF4eHMge1xyXG5cdEBtZWRpYSAobWF4LXdpZHRoOiAkeHMpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gbWF4c20ge1xyXG5cdEBtZWRpYSAobWF4LXdpZHRoOiAkc20pIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gbWF4bWQge1xyXG5cdEBtZWRpYSAobWF4LXdpZHRoOiAkbWQpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gbWF4bGcge1xyXG5cdEBtZWRpYSAobWF4LXdpZHRoOiAkbGcpIHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxufVxyXG5AbWl4aW4gb3JkZXIoJHZhbCkge1xyXG5cdC13ZWJraXQtYm94LW9yZGluYWwtZ3JvdXA6ICR2YWw7XHJcblx0LW1vei1ib3gtb3JkaW5hbC1ncm91cDogJHZhbDtcclxuXHQtbXMtZmxleC1vcmRlcjogJHZhbDtcclxuXHQtd2Via2l0LW9yZGVyOiAkdmFsO1xyXG5cdG9yZGVyOiAkdmFsO1xyXG59XHJcbkBtaXhpbiB0cmFuc2l0aW9uKCR0cmFuc2l0aW9uLi4uKSB7XHJcblx0LW1vei10cmFuc2l0aW9uOiAkdHJhbnNpdGlvbjtcclxuXHQtby10cmFuc2l0aW9uOiAkdHJhbnNpdGlvbjtcclxuXHQtd2Via2l0LXRyYW5zaXRpb246ICR0cmFuc2l0aW9uO1xyXG5cdHRyYW5zaXRpb246ICR0cmFuc2l0aW9uO1xyXG59XHJcbkBtaXhpbiB0cmFuc2l0aW9uLXByb3BlcnR5KCRwcm9wZXJ0eS4uLikge1xyXG5cdC1tb3otdHJhbnNpdGlvbi1wcm9wZXJ0eTogJHByb3BlcnR5O1xyXG5cdC1vLXRyYW5zaXRpb24tcHJvcGVydHk6ICRwcm9wZXJ0eTtcclxuXHQtd2Via2l0LXRyYW5zaXRpb24tcHJvcGVydHk6ICRwcm9wZXJ0eTtcclxuXHR0cmFuc2l0aW9uLXByb3BlcnR5OiAkcHJvcGVydHk7XHJcbn1cclxuQG1peGluIHRyYW5zaXRpb24tZHVyYXRpb24oJGR1cmF0aW9uLi4uKSB7XHJcblx0LW1vei10cmFuc2l0aW9uLXByb3BlcnR5OiAkZHVyYXRpb247XHJcblx0LW8tdHJhbnNpdGlvbi1wcm9wZXJ0eTogJGR1cmF0aW9uO1xyXG5cdC13ZWJraXQtdHJhbnNpdGlvbi1wcm9wZXJ0eTogJGR1cmF0aW9uO1xyXG5cdHRyYW5zaXRpb24tcHJvcGVydHk6ICRkdXJhdGlvbjtcclxufVxyXG5AbWl4aW4gdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb24oJHRpbWluZy4uLikge1xyXG5cdC1tb3otdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ICR0aW1pbmc7XHJcblx0LW8tdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ICR0aW1pbmc7XHJcblx0LXdlYmtpdC10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjogJHRpbWluZztcclxuXHR0cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjogJHRpbWluZztcclxufVxyXG5AbWl4aW4gdHJhbnNpdGlvbi1kZWxheSgkZGVsYXkuLi4pIHtcclxuXHQtbW96LXRyYW5zaXRpb24tZGVsYXk6ICRkZWxheTtcclxuXHQtby10cmFuc2l0aW9uLWRlbGF5OiAkZGVsYXk7XHJcblx0LXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAkZGVsYXk7XHJcblx0dHJhbnNpdGlvbi1kZWxheTogJGRlbGF5O1xyXG59XHJcbkBtaXhpbiB0cmFuc2Zvcm0oJGluKSB7XHJcblx0dHJhbnNmb3JtOiAkaW47XHJcblx0LXdlYmtpdC10cmFuc2Zvcm06ICRpbjtcclxuXHQtbW96LXRyYW5zZm9ybTogJGluO1xyXG5cdC1vLXRyYW5zZm9ybTogJGluO1xyXG5cdC1tcy10cmFuc2Zvcm06ICRpbjtcclxufVxyXG5AbWl4aW4gdHJhbnNmb3JtLXN0eWxlKCR0eXBlKSB7XHJcblx0LXdlYmtpdC10cmFuc2Zvcm0tc3R5bGU6ICR0eXBlO1xyXG5cdC1tb3otdHJhbnNmb3JtLXN0eWxlOiAkdHlwZTtcclxuXHQtby10cmFuc2Zvcm0tc3R5bGU6ICR0eXBlO1xyXG5cdC1tcy10cmFuc2Zvcm0tc3R5bGU6ICR0eXBlO1xyXG5cdHRyYW5zZm9ybS1zdHlsZTogJHR5cGU7XHJcbn1cclxuQG1peGluIGFuaW1hdGlvbigkYW5pbWF0ZS4uLikge1xyXG5cdCRtYXg6IGxlbmd0aCgkYW5pbWF0ZSk7XHJcblx0JGFuaW1hdGlvbnM6ICcnO1xyXG5cdEBmb3IgJGkgZnJvbSAxIHRocm91Z2ggJG1heCB7XHJcblx0XHQkYW5pbWF0aW9uczogI3skYW5pbWF0aW9ucyArIG50aCgkYW5pbWF0ZSwgJGkpfTtcclxuXHRcdEBpZiAkaSA8ICRtYXgge1xyXG5cdFx0XHQkYW5pbWF0aW9uczogI3skYW5pbWF0aW9ucyArIFwiLCBcIn07XHJcblx0XHR9XHJcblx0fVxyXG5cdC13ZWJraXQtYW5pbWF0aW9uOiAkYW5pbWF0aW9ucztcclxuXHQtbW96LWFuaW1hdGlvbjogJGFuaW1hdGlvbnM7XHJcblx0LW8tYW5pbWF0aW9uOiAkYW5pbWF0aW9ucztcclxuXHRhbmltYXRpb246ICRhbmltYXRpb25zO1xyXG59XHJcbkBtaXhpbiBhbmltYXRpb24tZHVyYXRpb24oJHRpbWUpIHtcclxuXHQtd2Via2l0LWFuaW1hdGlvbi1kdXJhdGlvbjogJHRpbWU7XHJcblx0LW1vei1hbmltYXRpb24tZHVyYXRpb246ICR0aW1lO1xyXG5cdC1tcy1hbmltYXRpb24tZHVyYXRpb246ICR0aW1lO1xyXG5cdC1vLWFuaW1hdGlvbi1kdXJhdGlvbjogJHRpbWU7XHJcblx0YW5pbWF0aW9uLWR1cmF0aW9uOiAkdGltZTtcclxufVxyXG5AbWl4aW4ga2V5ZnJhbWVzKCRhbmltYXRpb25OYW1lKSB7XHJcblx0QC13ZWJraXQta2V5ZnJhbWVzICN7JGFuaW1hdGlvbk5hbWV9IHtcclxuXHRcdEBjb250ZW50O1xyXG5cdH1cclxuXHRALW1vei1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG5cdEAtby1rZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG5cdEBrZXlmcmFtZXMgI3skYW5pbWF0aW9uTmFtZX0ge1xyXG5cdFx0QGNvbnRlbnQ7XHJcblx0fVxyXG59XHJcbkBtaXhpbiBub3NlbGVjdCgpIHtcclxuXHQtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XHJcblx0LyogaU9TIFNhZmFyaSAqL1xyXG5cdC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcblx0LyogU2FmYXJpICovXHJcblx0LWtodG1sLXVzZXItc2VsZWN0OiBub25lO1xyXG5cdC8qIEtvbnF1ZXJvciBIVE1MICovXHJcblx0LW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuXHQvKiBGaXJlZm94ICovXHJcblx0LW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG5cdC8qIEludGVybmV0IEV4cGxvcmVyL0VkZ2UgKi9cclxuXHR1c2VyLXNlbGVjdDogbm9uZTtcclxuXHQvKiBOb24tcHJlZml4ZWQgdmVyc2lvbiwgY3VycmVudGx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0ZWQgYnkgQ2hyb21lIGFuZCBPcGVyYSAqL1xyXG59XHJcblxyXG5cclxuQG1peGluIGRlZkJveFNoYWRvdygpIHtcclxuXHRib3gtc2hhZG93OiAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTIpLCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbn1cclxuXHJcblxyXG5AbWl4aW4gZGVmQm94U2hhZG93U3Ryb25nKCkge1xyXG5cdGJveC1zaGFkb3c6IDAgM3B4IDNweCAwIHJnYmEoMCwgMCwgMCwgMC41KSwgMCA0cHggMnB4IC0zcHggcmdiYSgwLCAwLCAwLCAwLjUpLCAwIDJweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuNCk7XHJcbn1cclxuXHJcbkBtaXhpbiBkZWZCb3hTaGFkb3dJbnNldCgpIHtcclxuXHRib3gtc2hhZG93OiBpbnNldCAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLCBpbnNldCAwIDNweCAxcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTIpLCBpbnNldCAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbn1cclxuXHJcbkBtaXhpbiBkZWZCb3JkZXIoKSB7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyNSk7XHJcbn1cclxuXHJcbkBtaXhpbiBkZWZIUigpIHtcclxuXHRib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG59XHJcblxyXG5cclxuQG1peGluIGJveFNoYWRvd0xpZ2h0KCkge1xyXG5cdGJveC1zaGFkb3c6IDAgMnB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksIDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcclxufVxyXG5cclxuXHJcbkBtaXhpbiBib3hTaGFkb3coKSB7XHJcblx0Ym94LXNoYWRvdzogMCA0cHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjI4KSwgMCA2cHggMnB4IC00cHggcmdiYSgwLCAwLCAwLCAwLjI4KSwgMCAycHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC40KTtcclxufVxyXG5cclxuQG1peGluIGJveFNoYWRvd1N0cm9uZygpIHtcclxuXHRib3gtc2hhZG93OiAwIDhweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMjgpLCAwIDEycHggNHB4IC04cHggcmdiYSgwLCAwLCAwLCAwLjI4KSwgMCA0cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC40KTtcclxufVxyXG5cclxuQG1peGluIGJveFNoYWRvd0luc2V0TGlnaHQoKSB7XHJcblx0Ym94LXNoYWRvdzogaW5zZXQgMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSwgaW5zZXQgMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjEyKSwgaW5zZXQgMCAxcHggNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG59XHJcblxyXG5AbWl4aW4gYm94U2hhZG93SW5zZXQoKSB7XHJcblx0Ym94LXNoYWRvdzogaW5zZXQgMCA0cHggNHB4IDAgcmdiYSgwLCAwLCAwLCAwLjI4KSwgaW5zZXQgMCA2cHggMnB4IC00cHggcmdiYSgwLCAwLCAwLCAwLjI0KSwgaW5zZXQgMCAycHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC40KTtcclxufVxyXG5cclxuQG1peGluIGJveFNoYWRvd0luc2V0U3Ryb25nKCkge1xyXG5cdGJveC1zaGFkb3c6IGluc2V0IDAgOHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yOCksIGluc2V0IDAgMTJweCA0cHggLTRweCByZ2JhKDAsIDAsIDAsIDAuMjQpLCBpbnNldCAwIDRweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjQpO1xyXG59XHJcblxyXG4iXX0= */"],
        data: {
          animation: [src_app_animations_main_animations__WEBPACK_IMPORTED_MODULE_2__["toggleHeight"]]
        }
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotificationFactoryComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'notification-factory',
            templateUrl: './notification-factory.component.html',
            styleUrls: ['./notification-factory.component.scss'],
            animations: [src_app_animations_main_animations__WEBPACK_IMPORTED_MODULE_2__["toggleHeight"]]
          }]
        }], function () {
          return [{
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_notification_service__WEBPACK_IMPORTED_MODULE_3__["NotificationService"]]
            }]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "ygmV":
    /*!****************************************!*\
      !*** ./src/app/data/level.strategy.ts ***!
      \****************************************/

    /*! exports provided: LevelStrategy */

    /***/
    function ygmV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LevelStrategy", function () {
        return LevelStrategy;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var LevelStrategy = /*#__PURE__*/function () {
        function LevelStrategy() {
          _classCallCheck(this, LevelStrategy);
        }

        _createClass(LevelStrategy, null, [{
          key: "maxExp",
          value: function maxExp(level) {
            return 100 + Math.pow(10 + level, .5);
          }
        }, {
          key: "maxMainExp",
          value: function maxMainExp(level) {
            return this.maxExp(level) * (30 - 25 / Math.pow(level + 1, .25));
          }
        }, {
          key: "perc",
          value: function perc(exp, level) {
            return exp / this.maxExp(level);
          }
        }]);

        return LevelStrategy;
      }();

      LevelStrategy.ɵfac = function LevelStrategy_Factory(t) {
        return new (t || LevelStrategy)();
      };

      LevelStrategy.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: LevelStrategy,
        factory: LevelStrategy.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LevelStrategy, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/

    },

    /***/
    "zn8P":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function zn8P(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "zn8P";
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map